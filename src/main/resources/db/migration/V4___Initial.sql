/*Category*/

/*Back-end Categories*/
INSERT INTO category (name) VALUES ('JAVA');
INSERT INTO category (name) VALUES ('JVM');
INSERT INTO category (name) VALUES ('Library');
INSERT INTO category (name) VALUES ('Web');
INSERT INTO category (name) VALUES ('Markup Languages');
INSERT INTO category (name) VALUES ('Spring Framework');
INSERT INTO category (name) VALUES ('Spring Security');
INSERT INTO category (name) VALUES ('Caching');
INSERT INTO category (name) VALUES ('Messaging');
INSERT INTO category (name) VALUES ('Persistence');
INSERT INTO category (name) VALUES ('Operations');
INSERT INTO category (name) VALUES ('Cloud');
INSERT INTO category (name) VALUES ('Scripting');


/*Front-end Categories*/
INSERT INTO category (name) VALUES ('Javascript');
INSERT INTO category (name) VALUES ('Typescript');
INSERT INTO category (name) VALUES ('HTML & CSS');
INSERT INTO category (name) VALUES ('Frameworks & Libraries');
INSERT INTO category (name) VALUES ('WebSockets');
INSERT INTO category (name) VALUES ('NPM & NodeJS');
INSERT INTO category (name) VALUES ('Authentication & Authorization');
INSERT INTO category (name) VALUES ('Tools & Operations');
INSERT INTO category (name) VALUES ('Webpack');

/*Common*/
INSERT INTO category (name) VALUES ('Testing');
INSERT INTO category (name) VALUES ('Version Control');
INSERT INTO category (name) VALUES ('Http & REST API');
INSERT INTO category (name) VALUES ('Network');
INSERT INTO category (name) VALUES ('DBMS');
INSERT INTO category (name) VALUES ('Data Structure & Algorithms');
INSERT INTO category (name) VALUES ('Software Architecture & Design');
INSERT INTO category (name) VALUES ('Design Patterns');


/*Topic*/

/*Back-End Topics*/

/*Java*/
INSERT INTO topic (name) VALUES ('Basics');
INSERT INTO topic (name) VALUES ('Collections');
INSERT INTO topic (name) VALUES ('Logging');
INSERT INTO topic (name) VALUES ('Java Date Time Api');
INSERT INTO topic (name) VALUES ('Concurrency and Multithreading');
INSERT INTO topic (name) VALUES ('Concurrency and Multithreading Advanced');

/*JVM*/
INSERT INTO topic (name) VALUES ('Specification');
INSERT INTO topic (name) VALUES ('Memory Management');
INSERT INTO topic (name) VALUES ('Threads and Locks');
INSERT INTO topic (name) VALUES ('Profiling and Performance Tuning');
INSERT INTO topic (name) VALUES ('Tuning');
INSERT INTO topic (name) VALUES ('Tuning Advanced');
INSERT INTO topic (name) VALUES ('Languages');

/*Library*/

INSERT INTO topic (name) VALUES ('Guava');
INSERT INTO topic (name) VALUES ('Jackson');
INSERT INTO topic (name) VALUES ('GSON');
INSERT INTO topic (name) VALUES ('Guava Collections');
INSERT INTO topic (name) VALUES ('Guava Caches');
INSERT INTO topic (name) VALUES ('RxJava');
INSERT INTO topic (name) VALUES ('Apache POI');
INSERT INTO topic (name) VALUES ('Guava Graphs');
INSERT INTO topic (name) VALUES ('Resilience4j');
INSERT INTO topic (name) VALUES ('Vavr');
INSERT INTO topic (name) VALUES ('Neo4j');


/*Web*/

INSERT INTO topic (name) VALUES ('Servlets and JSP');

/*Markup Languages*/

INSERT INTO topic (name) VALUES ('Exploring XML');
INSERT INTO topic (name) VALUES ('Exploring JSON');

/*Spring Framework*/

INSERT INTO topic (name) VALUES ('Core');
INSERT INTO topic (name) VALUES ('Testing');
INSERT INTO topic (name) VALUES ('Data Access');
INSERT INTO topic (name) VALUES ('Web Servlet');
INSERT INTO topic (name) VALUES ('Web Reactive');
INSERT INTO topic (name) VALUES ('Integration');
INSERT INTO topic (name) VALUES ('Languages');

/*Spring Security*/

INSERT INTO topic (name) VALUES ('Java Configuration');
INSERT INTO topic (name) VALUES ('Security Namespace Configuration');
INSERT INTO topic (name) VALUES ('Architecture and Implementation');
INSERT INTO topic (name) VALUES ('Testing');
INSERT INTO topic (name) VALUES ('Web Application Security');
INSERT INTO topic (name) VALUES ('Authorization');
INSERT INTO topic (name) VALUES ('Reactive Application Security');


/*Testing*/

INSERT INTO topic (name) VALUES ('Junit');
INSERT INTO topic (name) VALUES ('Mockito');
INSERT INTO topic (name) VALUES ('AssertJ');
INSERT INTO topic (name) VALUES ('Hamcrest');
INSERT INTO topic (name) VALUES ('WireMock');
INSERT INTO topic (name) VALUES ('Gatling');

/*Caching*/

INSERT INTO topic (name) VALUES ('Java Cache API');
INSERT INTO topic (name) VALUES ('EhCache');
INSERT INTO topic (name) VALUES ('Apache Commons JCS');
INSERT INTO topic (name) VALUES ('Cache2k');
INSERT INTO topic (name) VALUES ('Guava Caches');
INSERT INTO topic (name) VALUES ('Spring Cache');
INSERT INTO topic (name) VALUES ('Redis Cache');

/*Messaging*/

INSERT INTO topic (name) VALUES ('Java Message Service API');
INSERT INTO topic (name) VALUES ('RabbitMQ');
INSERT INTO topic (name) VALUES ('ActiveMQ');
INSERT INTO topic (name) VALUES ('Redis');
INSERT INTO topic (name) VALUES ('Kafka');
INSERT INTO topic (name) VALUES ('RocketMQ');

/*Persistence*/

INSERT INTO topic (name) VALUES ('JDBC');
INSERT INTO topic (name) VALUES ('Java Persistence API');
INSERT INTO topic (name) VALUES ('Java Data Objects API');
INSERT INTO topic (name) VALUES ('Hibernate');
INSERT INTO topic (name) VALUES ('ElasticSearch');
INSERT INTO topic (name) VALUES ('EclipseLink');
INSERT INTO topic (name) VALUES ('Apache OpenJPA');
INSERT INTO topic (name) VALUES ('ObjectDB');
INSERT INTO topic (name) VALUES ('Datanucleus');

/*Version Control*/

INSERT INTO topic (name) VALUES ('ObjectDB');
INSERT INTO topic (name) VALUES ('Datanucleus');

/*Operations*/

INSERT INTO topic (name) VALUES ('Maven Basics');
INSERT INTO topic (name) VALUES ('Gradle Basics');
INSERT INTO topic (name) VALUES ('Maven Advanced');
INSERT INTO topic (name) VALUES ('Gradle Advanced');
INSERT INTO topic (name) VALUES ('Docker');
INSERT INTO topic (name) VALUES ('Jenkins Pipelines');
INSERT INTO topic (name) VALUES ('Kubernetes');
INSERT INTO topic (name) VALUES ('Docker Advanced');
INSERT INTO topic (name) VALUES ('Kubernetes Advanced');
INSERT INTO topic (name) VALUES ('Maven Basics');

/*Cloud*/

INSERT INTO topic (name) VALUES ('Google Cloud Platform');
INSERT INTO topic (name) VALUES ('Amazon Web Services');
INSERT INTO topic (name) VALUES ('Azure');

/*Scripting*/

INSERT INTO topic (name) VALUES ('Command Line Basics');
INSERT INTO topic (name) VALUES ('Command Line Advanced');
INSERT INTO topic (name) VALUES ('Python');
INSERT INTO topic (name) VALUES ('Bash Scripting');


/*Http & REST API*/

INSERT INTO topic (name) VALUES ('HTTP Basics');
INSERT INTO topic (name) VALUES ('HTTP Advanced');
INSERT INTO topic (name) VALUES ('REST API Basics');
INSERT INTO topic (name) VALUES ('REST API Advanced');
INSERT INTO topic (name) VALUES ('Design RESTful API');

/*Network*/

INSERT INTO topic (name) VALUES ('Basics');
INSERT INTO topic (name) VALUES ('OSI Model');
INSERT INTO topic (name) VALUES ('IP Addressing');
INSERT INTO topic (name) VALUES ('IP Subnetting');

/*DBMS*/

INSERT INTO topic (name) VALUES ('Basics');
INSERT INTO topic (name) VALUES ('Entity Relationship Model');
INSERT INTO topic (name) VALUES ('Relational Model');
INSERT INTO topic (name) VALUES ('Relational Database Design');
INSERT INTO topic (name) VALUES ('Storage and File Structure');
INSERT INTO topic (name) VALUES ('Indexing and Hashing');
INSERT INTO topic (name) VALUES ('Transaction And Concurrency');
INSERT INTO topic (name) VALUES ('Backup and Recovery');

/*Data Structure & Algorithms*/

INSERT INTO topic (name) VALUES ('Algorithms Basics');
INSERT INTO topic (name) VALUES ('Data Structure Basics');
INSERT INTO topic (name) VALUES ('Data Structure Advanced');
INSERT INTO topic (name) VALUES ('Search Algorithms');
INSERT INTO topic (name) VALUES ('Sorting Algorithms');
INSERT INTO topic (name) VALUES ('Graph');
INSERT INTO topic (name) VALUES ('Tree');
INSERT INTO topic (name) VALUES ('Algorithms Advanced');

/*Software Architecture & Design*/

INSERT INTO topic (name) VALUES ('Object-Oriented Paradigm');
INSERT INTO topic (name) VALUES ('Key Principles');
INSERT INTO topic (name) VALUES ('Architecture Models');
INSERT INTO topic (name) VALUES ('Data Flow Architecture');
INSERT INTO topic (name) VALUES ('Data-Centered Architecture');
INSERT INTO topic (name) VALUES ('Hierarchical Architecture');
INSERT INTO topic (name) VALUES ('Interaction-Oriented Architecture');
INSERT INTO topic (name) VALUES ('Component-Based Architecture');
INSERT INTO topic (name) VALUES ('Distributed Architecture');
INSERT INTO topic (name) VALUES ('User Interface');
INSERT INTO topic (name) VALUES ('Architecture Techniques');

/*Design Patterns*/

INSERT INTO topic (name) VALUES ('Intro to Design Patterns');
INSERT INTO topic (name) VALUES ('The Observer Pattern');
INSERT INTO topic (name) VALUES ('The Decorator Pattern');
INSERT INTO topic (name) VALUES ('The Factory Pattern');
INSERT INTO topic (name) VALUES ('The Singleton Pattern');
INSERT INTO topic (name) VALUES ('The Bridge Pattern');
INSERT INTO topic (name) VALUES ('The Builder Pattern');
INSERT INTO topic (name) VALUES ('The Flyweight Pattern');
INSERT INTO topic (name) VALUES ('The Interpreter Pattern');
INSERT INTO topic (name) VALUES ('The Mediator Pattern');
INSERT INTO topic (name) VALUES ('The Memento Pattern');
INSERT INTO topic (name) VALUES ('The Prototype Pattern');
INSERT INTO topic (name) VALUES ('The Visitor Pattern');
INSERT INTO topic (name) VALUES ('The Command Pattern');
INSERT INTO topic (name) VALUES ('The Adapter and Facade Patterns');
INSERT INTO topic (name) VALUES ('The Template Method Pattern');
INSERT INTO topic (name) VALUES ('The Iterator and Composite Patterns');
INSERT INTO topic (name) VALUES ('The State Pattern');
INSERT INTO topic (name) VALUES ('The Proxy Pattern');
INSERT INTO topic (name) VALUES ('Compound Patterns');
INSERT INTO topic (name) VALUES ('Patterns in the Real World');


/*Front-end topic*/

/*Javascript*/

INSERT INTO topic (name) VALUES ('The JavaScript language');
INSERT INTO topic (name) VALUES ('Browser: Document, Events, Interfaces');
INSERT INTO topic (name) VALUES ('Frames and windows');
INSERT INTO topic (name) VALUES ('Promises, async/await');
INSERT INTO topic (name) VALUES ('Network requests');
INSERT INTO topic (name) VALUES ('Animation');
INSERT INTO topic (name) VALUES ('Regular expressions');

/*Typescript*/

INSERT INTO topic (name) VALUES ('The basic of Typescript');
INSERT INTO topic (name) VALUES ('Typescript modules');
INSERT INTO topic (name) VALUES ('Declaration Files');
INSERT INTO topic (name) VALUES ('Project Configuration');


/*Html & css*/

INSERT INTO topic (name) VALUES ('Basics');
INSERT INTO topic (name) VALUES ('Basics Part 2');
INSERT INTO topic (name) VALUES ('Advanced');
INSERT INTO topic (name) VALUES ('Advanced Part 2');
INSERT INTO topic (name) VALUES ('CSS Architecture');

/*Frameworks & Libraries*/

INSERT INTO topic (name) VALUES ('jQuery Basics');
INSERT INTO topic (name) VALUES ('Angular Basics');
INSERT INTO topic (name) VALUES ('Angular Material Basics');
INSERT INTO topic (name) VALUES ('Angular Material Advanced');
INSERT INTO topic (name) VALUES ('Angular Advanced');


/*Http & REST API*/

INSERT INTO topic (name) VALUES ('HTTP Basics');
INSERT INTO topic (name) VALUES ('HTTP Advanced');
INSERT INTO topic (name) VALUES ('REST API Basics');
INSERT INTO topic (name) VALUES ('REST API Advanced');
INSERT INTO topic (name) VALUES ('Design RESTful API');


/*WebSockets*/

INSERT INTO topic (name) VALUES ('WebSockets API');
INSERT INTO topic (name) VALUES ('Socket IO');
INSERT INTO topic (name) VALUES ('SockJS');


/*Npm & NodeJS*/

INSERT INTO topic (name) VALUES ('Introduction to packages and modules');
INSERT INTO topic (name) VALUES ('Contributing packages to the registry');
INSERT INTO topic (name) VALUES ('Updating and managing packages');
INSERT INTO topic (name) VALUES ('NodeJS Basics');
INSERT INTO topic (name) VALUES ('NodeJS Advanced');

/*Authentication & Authorization*/

INSERT INTO topic (name) VALUES ('OAuth2.0');

/*Tools & Operations*/

INSERT INTO topic (name) VALUES ('TSLint Usage');
INSERT INTO topic (name) VALUES ('TSLint Rules');
INSERT INTO topic (name) VALUES ('Gulp Basics');
INSERT INTO topic (name) VALUES ('TSLint Formatters');
INSERT INTO topic (name) VALUES ('Gulp Advanced');
INSERT INTO topic (name) VALUES ('Docker');
INSERT INTO topic (name) VALUES ('Jenkins Pipelines');
INSERT INTO topic (name) VALUES ('Kubernetes');

/*Webpack*/

INSERT INTO topic (name) VALUES ('Concepts');
INSERT INTO topic (name) VALUES ('Entry Points');
INSERT INTO topic (name) VALUES ('Output');
INSERT INTO topic (name) VALUES ('Mode');
INSERT INTO topic (name) VALUES ('Loaders');
INSERT INTO topic (name) VALUES ('Plugins');
INSERT INTO topic (name) VALUES ('Configuration');
INSERT INTO topic (name) VALUES ('Modules');
INSERT INTO topic (name) VALUES ('Module Resolution');

/*Version Control*/

INSERT INTO topic (name) VALUES ('Git Basics');
INSERT INTO topic (name) VALUES ('Git Advanced');

/*Testing*/

INSERT INTO topic (name) VALUES ('Software Testing');
INSERT INTO topic (name) VALUES ('JEST');
INSERT INTO topic (name) VALUES ('JasmineJS');
INSERT INTO topic (name) VALUES ('Karma');
INSERT INTO topic (name) VALUES ('Security Testing');
INSERT INTO topic (name) VALUES ('Selenium Tutorial');
INSERT INTO topic (name) VALUES ('Protractor');

/*DBMS*/

INSERT INTO topic (name) VALUES ('Basics');
INSERT INTO topic (name) VALUES ('Entity Relationship Model');
INSERT INTO topic (name) VALUES ('Relational Model');
INSERT INTO topic (name) VALUES ('Relational Database Design');
INSERT INTO topic (name) VALUES ('Storage and File Structure');
INSERT INTO topic (name) VALUES ('Indexing and Hashing');
INSERT INTO topic (name) VALUES ('Transaction And Concurrency');
INSERT INTO topic (name) VALUES ('Backup and Recovery');


/*Data Structure & Algorithms*/

INSERT INTO topic (name) VALUES ('Algorithms Basics');
INSERT INTO topic (name) VALUES ('Data Structure Basics');
INSERT INTO topic (name) VALUES ('Data Structure Advanced');
INSERT INTO topic (name) VALUES ('Search Algorithms');
INSERT INTO topic (name) VALUES ('Sorting Algorithms');
INSERT INTO topic (name) VALUES ('Graph');
INSERT INTO topic (name) VALUES ('Tree');
INSERT INTO topic (name) VALUES ('Algorithms Advanced');

/*Software Architecture & Design*/

INSERT INTO topic (name) VALUES ('Object-Oriented Paradigm');
INSERT INTO topic (name) VALUES ('Key Principles');
INSERT INTO topic (name) VALUES ('Architecture Models');
INSERT INTO topic (name) VALUES ('Data Flow Architecture');
INSERT INTO topic (name) VALUES ('Data-Centered Architecture');
INSERT INTO topic (name) VALUES ('Hierarchical Architecture');
INSERT INTO topic (name) VALUES ('Interaction-Oriented Architecture');
INSERT INTO topic (name) VALUES ('Component-Based Architecture');
INSERT INTO topic (name) VALUES ('Distributed Architecture');
INSERT INTO topic (name) VALUES ('User Interface');
INSERT INTO topic (name) VALUES ('Architecture Techniques');

/*Design Patterns*/

INSERT INTO topic (name) VALUES ('Design Patterns');
INSERT INTO topic (name) VALUES ('Design Patterns');
INSERT INTO topic (name) VALUES ('MV* Patterns');
INSERT INTO topic (name) VALUES ('Modular Design Patterns');
INSERT INTO topic (name) VALUES ('Namespacing Patterns');



