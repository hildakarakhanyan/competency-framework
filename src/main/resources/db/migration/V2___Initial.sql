CREATE TABLE if not EXISTS "role"
(
  id       int8 NOT NULL,
  name VARCHAR(255),

  PRIMARY KEY (id)
);

INSERT INTO role (id, name) VALUES (1,'Admin');
INSERT INTO role (id, name) VALUES (2,'Team_Lead');
INSERT INTO role (id, name) VALUES (3,'Public');
