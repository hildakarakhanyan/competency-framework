CREATE sequence hibernate_sequence start 1 increment 1;

CREATE TABLE if not EXISTS "user"
(
  id       int8 NOT NULL,
  password VARCHAR(255),
  username VARCHAR(255),

  PRIMARY KEY (id)
);

CREATE TABLE if NOT EXISTS framework_type (
  id       int8 NOT NULL,
  name varchar(255),
  PRIMARY KEY (id)
);

INSERT INTO framework_type (id, name) VALUES (1,'Back-end development');
INSERT INTO framework_type (id, name) VALUES (2,'Front-end development');
INSERT INTO framework_type (id, name) VALUES (3,'UI developer');
INSERT INTO framework_type (id, name) VALUES (4,'QA automation');
INSERT INTO framework_type (id, name) VALUES (5,'DB development');
INSERT INTO framework_type (id, name) VALUES (6,'DBA');
INSERT INTO framework_type (id, name) VALUES (7,'QA manual');
INSERT INTO framework_type (id, name) VALUES (8,'System administration');
INSERT INTO framework_type (id, name) VALUES (9,'Project management');
INSERT INTO framework_type (id, name) VALUES (10,'Documentation');
INSERT INTO framework_type (id, name) VALUES (11,'Support');
INSERT INTO framework_type (id, name) VALUES (12,'DevOps');