ALTER TABLE "framework_type" RENAME TO framework;

CREATE SEQUENCE state_id_seq;

CREATE TABLE if not EXISTS "state"
(
  id       int8 NOT NULL DEFAULT nextval('state_id_seq'),
  name VARCHAR(255),
  PRIMARY KEY (id)
);

ALTER SEQUENCE state_id_seq
    OWNED BY state.id;




CREATE SEQUENCE category_id_seq;

CREATE TABLE if NOT EXISTS "category" (
  id       int8 NOT NULL DEFAULT nextval('category_id_seq'),
  name varchar(255),
  PRIMARY KEY (id)
);
ALTER SEQUENCE category_id_seq
    OWNED BY category.id;



CREATE SEQUENCE topic_id_seq;


CREATE TABLE if NOT EXISTS "topic" (
  id       int8  NOT NULL DEFAULT nextval('topic_id_seq'),
  name varchar(255),
  description varchar(255),
  PRIMARY KEY (id)
);

ALTER SEQUENCE topic_id_seq
    OWNED BY topic.id;



CREATE SEQUENCE topic_detail_id_seq;

CREATE TABLE if NOT EXISTS "topic_detail" (
  id       int8 NOT NULL DEFAULT nextval('topic_detail_id_seq'),
  url varchar(512),
  description varchar(512),
  topic_Id int8,
  CONSTRAINT fk_topicDetail_topic FOREIGN KEY (topic_Id)
  REFERENCES topic(id),
  PRIMARY KEY (id)
);

ALTER SEQUENCE topic_detail_id_seq
    OWNED BY topic_detail.id;


CREATE TYPE level AS ENUM ('First', 'Second', 'Third', 'Forth', 'Fifth', 'Sixth', 'Seventh', 'Eight', 'Ninth');


CREATE SEQUENCE topic_framework_id_seq;


CREATE TABLE if NOT EXISTS "topic_framework" (
  id       int8 NOT NULL DEFAULT nextval('topic_framework_id_seq'),
  framework_Id int8,
  category_Id int8,
  topic_Id int8,
  level level,
  CONSTRAINT fk_topicFramework_framework FOREIGN KEY (framework_Id)
  REFERENCES framework(id),
  CONSTRAINT fk_topicFramework_category FOREIGN KEY (category_Id)
  REFERENCES category(id),
  CONSTRAINT fk_topicFramework_topic FOREIGN KEY (topic_Id)
  REFERENCES topic(id),
  PRIMARY KEY (id)
);


ALTER SEQUENCE topic_framework_id_seq
    OWNED BY topic_framework.id;



CREATE SEQUENCE assessment_id_seq;

CREATE TABLE if NOT EXISTS "assessment" (
  id       int8 NOT NULL DEFAULT nextval('assessment_id_seq'),
  description varchar(512),
  topic_Framework_Id int8,
  user_Id int8,
  state_Id int8,
  assessment_date timestamp,
  CONSTRAINT fk_assessment_topicFramework FOREIGN KEY (topic_framework_Id)
  REFERENCES topic_framework(id),
  CONSTRAINT fk_assessment_user FOREIGN KEY (user_Id)
  REFERENCES "user"(id),
  CONSTRAINT fk_assessment_state FOREIGN KEY (state_Id)
  REFERENCES state(id),
  PRIMARY KEY (id)
);

ALTER SEQUENCE assessment_id_seq
    OWNED BY assessment.id;

