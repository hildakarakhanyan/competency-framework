/*Topic__Detail*/

/*Back-end*/

/*Java*/

/*Basics*/

INSERT INTO topic_detail (topic_Id, url, description) VALUES (1,'https://www.oreilly.com/library/view/head-first-java/0596009208/ch01.html','Java Language');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (1,'https://www.oreilly.com/library/view/head-first-java/0596009208/ch02.html','Classes and Objects');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (1,'https://www.oreilly.com/library/view/head-first-java/0596009208/ch03.html','Primitives and References');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (1,'https://www.oreilly.com/library/view/head-first-java/0596009208/ch04.html','How Objects Behave');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (1,'https://www.oreilly.com/library/view/head-first-java/0596009208/ch05.html','Extra-Strength Methods');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (1,'https://www.oreilly.com/library/view/head-first-java/0596009208/ch06.html','Using the Java Library');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (1,'https://www.oreilly.com/library/view/head-first-java/0596009208/ch07.html','Inheritance and Polymorphism');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (1,'https://www.oreilly.com/library/view/head-first-java/0596009208/ch08.html','Interfaces and Abstract Classes');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (1,'https://www.oreilly.com/library/view/head-first-java/0596009208/ch09.html','Life and Death of an Object');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (1,'https://www.oreilly.com/library/view/head-first-java/0596009208/ch10.html','Numbers and Statics');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (1,'https://www.oreilly.com/library/view/head-first-java/0596009208/ch11.html','Exception Handling');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (1,'https://www.oreilly.com/library/view/head-first-java/0596009208/ch14.html','Serialization and File I/O');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (1,'https://www.oreilly.com/library/view/head-first-java/0596009208/ch15.html','Networking and Threads');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (1,'https://www.oreilly.com/library/view/head-first-java/0596009208/ch16.html','Collections and Generics');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (1,'https://www.oreilly.com/library/view/head-first-java/0596009208/ch17.html','Package, Jars and Deployment');


/*Collections*/


INSERT INTO topic_detail (topic_Id, url, description) VALUES (2,'https://docs.oracle.com/javase/tutorial/collections/interfaces/collection.html','The Collection Interface');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (2,'https://docs.oracle.com/javase/tutorial/collections/interfaces/set.html','The Set Interface');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (2,'https://docs.oracle.com/javase/tutorial/collections/interfaces/list.html','The List Interface');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (2,'https://docs.oracle.com/javase/tutorial/collections/interfaces/queue.html','The Queue Interface');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (2,'https://docs.oracle.com/javase/tutorial/collections/interfaces/deque.html','The Deque Interface');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (2,'https://docs.oracle.com/javase/tutorial/collections/interfaces/map.html','The Map Interface');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (2,'https://docs.oracle.com/javase/tutorial/collections/interfaces/order.html','Object Ordering');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (2,'https://docs.oracle.com/javase/tutorial/collections/interfaces/sorted-set.html','The SortedSet Interface');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (2,'https://docs.oracle.com/javase/tutorial/collections/interfaces/sorted-map.html','The SortedMap Interface');

INSERT INTO topic_detail (topic_Id, url, description) VALUES (2,'https://docs.oracle.com/javase/tutorial/collections/streams/reduction.html','Reduction');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (2,'https://docs.oracle.com/javase/tutorial/collections/streams/parallelism.html','Parallelism');

INSERT INTO topic_detail (topic_Id, url, description) VALUES (2,'https://docs.oracle.com/javase/tutorial/collections/implementations/set.html','Set Implementations');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (2,'https://docs.oracle.com/javase/tutorial/collections/implementations/list.html','List Implementations');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (2,'https://docs.oracle.com/javase/tutorial/collections/implementations/map.html','Map Implementations');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (2,'https://docs.oracle.com/javase/tutorial/collections/implementations/queue.html','Queue Implementations');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (2,'https://docs.oracle.com/javase/tutorial/collections/implementations/deque.html','Deque Implementations');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (2,'https://docs.oracle.com/javase/tutorial/collections/implementations/wrapper.html','Wrapper Implementations');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (2,'https://docs.oracle.com/javase/tutorial/collections/implementations/convenience.html','Convenience Implementations');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (2,'https://docs.oracle.com/javase/tutorial/collections/custom-implementations/index.html','Custom Collection Implementations');

INSERT INTO topic_detail (topic_Id, url, description) VALUES (2,'https://docs.oracle.com/javase/tutorial/collections/algorithms/index.html#sorting','Sorting');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (2,'https://docs.oracle.com/javase/tutorial/collections/algorithms/index.html#shuffling','Shuffling');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (2,'https://docs.oracle.com/javase/tutorial/collections/algorithms/index.html#rdm','Routine Data Manipulation');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (2,'https://docs.oracle.com/javase/tutorial/collections/algorithms/index.html#searching','Searching');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (2,'https://docs.oracle.com/javase/tutorial/collections/algorithms/index.html#composition','Composition');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (2,'https://docs.oracle.com/javase/tutorial/collections/algorithms/index.html#fev','Finding Extreme Values');

INSERT INTO topic_detail (topic_Id, url, description) VALUES (2,'https://docs.oracle.com/javase/tutorial/collections/interoperability/compatibility.html','Compatibility');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (2,'https://docs.oracle.com/javase/tutorial/collections/interoperability/api-design.html','API Design');


/*Logging*/

INSERT INTO topic_detail (topic_Id, url, description) VALUES (3,'http://tutorials.jenkov.com/java-logging/index.html','Java Logging');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (3,'http://tutorials.jenkov.com/java-logging/overview.html','Java Logging: Overview');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (3,'http://tutorials.jenkov.com/java-logging/basic-usage.html','Java Logging: Basic Usage');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (3,'http://tutorials.jenkov.com/java-logging/logger.html','Java Logging: Logger');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (3,'http://tutorials.jenkov.com/java-logging/logger-hierarchy.html','Java Logging: Logger Hierarchy');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (3,'http://tutorials.jenkov.com/java-logging/levels.html','Java Logging: Log Levels');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (3,'http://tutorials.jenkov.com/java-logging/formatters.html','Java Logging: Formatters');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (3,'http://tutorials.jenkov.com/java-logging/filters.html','Java Logging: Filters');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (3,'http://tutorials.jenkov.com/java-logging/handlers.html','Java Logging: Handlers');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (3,'http://tutorials.jenkov.com/java-logging/logrecord.html','Java Logging: LogRecord');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (3,'http://tutorials.jenkov.com/java-logging/configuration.html','Java Logging: Configuration');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (3,'http://tutorials.jenkov.com/java-logging/logmanager.html','Java Logging: LogManager');


/*Java Date Time Api*/

INSERT INTO topic_detail (topic_Id, url, description) VALUES (4,'https://www.baeldung.com/java-8-date-time-intro','Introduction');



/*Concurrency and Multithreading*/

INSERT INTO topic_detail (topic_Id, url, description) VALUES (5,'http://tutorials.jenkov.com/java-concurrency/benefits.html','Multithreading Benefits');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (5,'http://tutorials.jenkov.com/java-concurrency/costs.html','Multithreading Costs');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (5,'http://tutorials.jenkov.com/java-concurrency/concurrency-models.html','Concurrency Models');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (5,'http://tutorials.jenkov.com/java-concurrency/same-threading.html','Same-threading');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (5,'http://tutorials.jenkov.com/java-concurrency/concurrency-vs-parallelism.html','Concurrency vs. Parallelism');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (5,'http://tutorials.jenkov.com/java-concurrency/creating-and-starting-threads.html','Creating and Starting Java Threads');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (5,'http://tutorials.jenkov.com/java-concurrency/race-conditions-and-critical-sections.html','Race Conditions and Critical Sections');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (5,'http://tutorials.jenkov.com/java-concurrency/thread-safety.html','Thread Safety and Shared Resources');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (5,'http://tutorials.jenkov.com/java-concurrency/thread-safety-and-immutability.html','Thread Safety and Immutability');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (5,'http://tutorials.jenkov.com/java-concurrency/java-memory-model.html','Java Memory Model');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (5,'http://tutorials.jenkov.com/java-concurrency/synchronized.html','Java Synchronized Blocks');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (5,'http://tutorials.jenkov.com/java-concurrency/volatile.html','Java Volatile Keyword');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (5,'http://tutorials.jenkov.com/java-concurrency/threadlocal.html','Java ThreadLocal');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (5,'http://tutorials.jenkov.com/java-concurrency/thread-signaling.html','Java Thread Signaling');



/*Concurrency and Multithreading Advanced*/

INSERT INTO topic_detail (topic_Id, url, description) VALUES (6,'http://tutorials.jenkov.com/java-concurrency/deadlock.html','Deadlock');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (6,'http://tutorials.jenkov.com/java-concurrency/deadlock-prevention.html','Deadlock Prevention');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (6,'http://tutorials.jenkov.com/java-concurrency/starvation-and-fairness.html','Starvation and Fairness');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (6,'http://tutorials.jenkov.com/java-concurrency/nested-monitor-lockout.html','Nested Monitor Lockout');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (6,'http://tutorials.jenkov.com/java-concurrency/slipped-conditions.html','Slipped Conditions');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (6,'http://tutorials.jenkov.com/java-concurrency/locks.html','Locks in Java');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (6,'http://tutorials.jenkov.com/java-concurrency/read-write-locks.html','Read / Write Locks in Java');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (6,'http://tutorials.jenkov.com/java-concurrency/reentrance-lockout.html','Reentrance Lockout');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (6,'http://tutorials.jenkov.com/java-concurrency/semaphores.html','Semaphores');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (6,'http://tutorials.jenkov.com/java-concurrency/blocking-queues.html','Blocking Queues');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (6,'http://tutorials.jenkov.com/java-concurrency/thread-pools.html','Thread Pools');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (6,'http://tutorials.jenkov.com/java-concurrency/compare-and-swap.html','Compare and Swap');


/*JVM*/

/*Specification*/


INSERT INTO topic_detail (topic_Id, url, description) VALUES (7,'https://docs.oracle.com/javase/specs/jvms/se8/html/jvms-1.html','Introduction');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (7,'https://docs.oracle.com/javase/specs/jvms/se8/html/jvms-2.html','The Structure of the Java Virtual Machine');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (7,'https://docs.oracle.com/javase/specs/jvms/se8/html/jvms-3.html','Compiling for the Java Virtual Machine');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (7,'https://docs.oracle.com/javase/specs/jvms/se8/html/jvms-4.html','The class File Format');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (7,'https://docs.oracle.com/javase/specs/jvms/se8/html/jvms-5.html','Loading, Linking, and Initializing');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (7,'https://docs.oracle.com/javase/specs/jvms/se8/html/jvms-6.html','The Java Virtual Machine Instruction Set');


/*Memory Management*/

INSERT INTO topic_detail (topic_Id, url, description) VALUES (8,'https://docs.oracle.com/cd/E13150_01/jrockit_jvm/jrockit/geninfo/diagnos/garbage_collect.html#wp1085825','The Heap and the Nursery');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (8,'https://docs.oracle.com/cd/E13150_01/jrockit_jvm/jrockit/geninfo/diagnos/garbage_collect.html#wp1085990','Object Allocation');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (8,'https://docs.oracle.com/cd/E13150_01/jrockit_jvm/jrockit/geninfo/diagnos/garbage_collect.html#wp1086087','Garbage Collection');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (8,'https://docs.oracle.com/cd/E13150_01/jrockit_jvm/jrockit/geninfo/diagnos/garbage_collect.html#wp1085786','The Mark and Sweep Model');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (8,'https://docs.oracle.com/cd/E13150_01/jrockit_jvm/jrockit/geninfo/diagnos/garbage_collect.html#wp1086786','Generational Garbage Collection');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (8,'https://docs.oracle.com/cd/E13150_01/jrockit_jvm/jrockit/geninfo/diagnos/garbage_collect.html#wp1086732','Dynamic and Static Garbage Collection Modes');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (8,'https://docs.oracle.com/cd/E13150_01/jrockit_jvm/jrockit/geninfo/diagnos/garbage_collect.html#wp1086917','Compaction');


/*Threads and Locks*/


INSERT INTO topic_detail (topic_Id, url, description) VALUES (9,'https://docs.oracle.com/cd/E13150_01/jrockit_jvm/jrockit/geninfo/diagnos/thread_basics.html#wp1094805','Understanding Threads');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (9,'https://docs.oracle.com/cd/E13150_01/jrockit_jvm/jrockit/geninfo/diagnos/thread_basics.html#wp1090499','Understanding Locks');

/*Profiling and Performance Tuning*/

INSERT INTO topic_detail (topic_Id, url, description) VALUES (10,'https://docs.oracle.com/cd/E13150_01/jrockit_jvm/jrockit/geninfo/diagnos/tuning_tradeoffs.html#wp1087715','Pause Times vs. Throughput');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (10,'https://docs.oracle.com/cd/E13150_01/jrockit_jvm/jrockit/geninfo/diagnos/tuning_tradeoffs.html#wp1087629','Concurrent vs. “Stop-the-World”');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (10,'https://docs.oracle.com/cd/E13150_01/jrockit_jvm/jrockit/geninfo/diagnos/tuning_tradeoffs.html#wp1087645','Compaction Pauses vs. Throughput');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (10,'https://docs.oracle.com/cd/E13150_01/jrockit_jvm/jrockit/geninfo/diagnos/tuning_tradeoffs.html#wp1087679','Performance vs. Memory Footprint');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (10,'https://docs.oracle.com/cd/E13150_01/jrockit_jvm/jrockit/geninfo/diagnos/tuning_tradeoffs.html#wp1087684','Heap Size vs. Throughput');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (10,'https://docs.oracle.com/cd/E13150_01/jrockit_jvm/jrockit/geninfo/diagnos/tuning_tradeoffs.html#wp1087694','Book Keeping vs. Pause Times');


/*Tuning*/


INSERT INTO topic_detail (topic_Id, url, description) VALUES (11,'https://docs.oracle.com/cd/E13150_01/jrockit_jvm/jrockit/geninfo/diagnos/bestpractices.html#wp1089635','Step 1: Basic Tuning');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (11,'https://docs.oracle.com/cd/E13150_01/jrockit_jvm/jrockit/geninfo/diagnos/bestpractices.html#wp1089681','Tuning the Heap Size');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (11,'https://docs.oracle.com/cd/E13150_01/jrockit_jvm/jrockit/geninfo/diagnos/bestpractices.html#wp1089709','Tuning the Garbage Collection');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (11,'https://docs.oracle.com/cd/E13150_01/jrockit_jvm/jrockit/geninfo/diagnos/bestpractices.html#wp1089749','Tuning the Nursery Size');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (11,'https://docs.oracle.com/cd/E13150_01/jrockit_jvm/jrockit/geninfo/diagnos/bestpractices.html#wp1089812','Tuning the Pause Target');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (11,'https://docs.oracle.com/cd/E13150_01/jrockit_jvm/jrockit/geninfo/diagnos/bestpractices.html#wp1089834','Step 2: Performance Tuning');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (11,'https://docs.oracle.com/cd/E13150_01/jrockit_jvm/jrockit/geninfo/diagnos/bestpractices.html#wp1089639','Lazy Unlocking');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (11,'https://docs.oracle.com/cd/E13150_01/jrockit_jvm/jrockit/geninfo/diagnos/bestpractices.html#wp1090728','Call Profiling');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (11,'https://docs.oracle.com/cd/E13150_01/jrockit_jvm/jrockit/geninfo/diagnos/bestpractices.html#wp1089874','Large Pages');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (11,'https://docs.oracle.com/cd/E13150_01/jrockit_jvm/jrockit/geninfo/diagnos/bestpractices.html#wp1089925','Step 3: Advanced Tuning');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (11,'https://docs.oracle.com/cd/E13150_01/jrockit_jvm/jrockit/geninfo/diagnos/bestpractices.html#wp1089934','Tuning Compaction');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (11,'https://docs.oracle.com/cd/E13150_01/jrockit_jvm/jrockit/geninfo/diagnos/bestpractices.html#wp1089942','Tuning the TLA size');


/*Tuning Advanced*/

INSERT INTO topic_detail (topic_Id, url, description) VALUES (12,'https://docs.oracle.com/cd/E13150_01/jrockit_jvm/jrockit/geninfo/diagnos/memman.html','Tuning the Memory Management System');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (12,'https://docs.oracle.com/cd/E13150_01/jrockit_jvm/jrockit/geninfo/diagnos/locktuning.html','Tuning Locks');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (12,'https://docs.oracle.com/cd/E13150_01/jrockit_jvm/jrockit/geninfo/diagnos/tune_fast_xaction.html','Tuning For Low Latencies');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (12,'https://docs.oracle.com/cd/E13150_01/jrockit_jvm/jrockit/geninfo/diagnos/tune_app_thruput.html','Tuning For Better Application Throughput');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (12,'https://docs.oracle.com/cd/E13150_01/jrockit_jvm/jrockit/geninfo/diagnos/tune_stable_perf.html','Tuning For Stable Performance');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (12,'https://docs.oracle.com/cd/E13150_01/jrockit_jvm/jrockit/geninfo/diagnos/tune_footprint.html','Tuning For a Small Memory Footprint');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (12,'https://docs.oracle.com/cd/E13150_01/jrockit_jvm/jrockit/geninfo/diagnos/tune_startup.html','Tuning For Faster JVM Startup');

/*Languages*/

INSERT INTO topic_detail (topic_Id, url, description) VALUES (13,'https://docs.scala-lang.org/tour/tour-of-scala.html','Scala');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (13,'https://kotlinlang.org/docs/reference/','Kotlin');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (13,'http://groovy-lang.org/documentation.html','Groovy');


/*Library*/
/*Guava*/

INSERT INTO topic_detail (topic_Id, url, description) VALUES (14,'https://github.com/google/guava/wiki/UsingAndAvoidingNullExplained','Using/avoiding null');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (14,'https://github.com/google/guava/wiki/PreconditionsExplained','Preconditions');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (14,'https://github.com/google/guava/wiki/OrderingExplained','Ordering');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (14,'https://github.com/google/guava/wiki/CommonObjectUtilitiesExplained','Object methods');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (14,'https://github.com/google/guava/wiki/ThrowablesExplained','Throwables');



/*Jackson*/


INSERT INTO topic_detail (topic_Id, url, description) VALUES (15,'http://tutorials.jenkov.com/java-json/jackson-objectmapper.html','Jackson ObjectMapper');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (15,'http://tutorials.jenkov.com/java-json/jackson-jsonnode.html','Jackson JsonNode');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (15,'http://tutorials.jenkov.com/java-json/jackson-jsonparser.html','Jackson JsonParser');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (15,'http://tutorials.jenkov.com/java-json/jackson-jsongenerator.html','Jackson JsonGenerator');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (15,'http://tutorials.jenkov.com/java-json/jackson-annotations.html','Jackson Annotations');


/*GSON*/

INSERT INTO topic_detail (topic_Id, url, description) VALUES (16,'http://tutorials.jenkov.com/java-json/gson.html','GSON - Gson');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (16,'http://tutorials.jenkov.com/java-json/gson-jsonreader.html','GSON - JsonReader');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (16,'http://tutorials.jenkov.com/java-json/gson-jsonparser.html','GSON - JsonParser');

/*RxJava*/

INSERT INTO topic_detail (topic_Id, url, description) VALUES (19,'https://github.com/ReactiveX/RxJava/wiki/How-To-Use-RxJava','How to Use RxJava');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (19,'https://github.com/ReactiveX/RxJava/wiki/Reactive-Streams','Reactive Streams');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (19,'https://github.com/ReactiveX/RxJava/wiki/Observable','The reactive types of RxJava');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (19,'https://github.com/ReactiveX/RxJava/wiki/Scheduler','Schedulers');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (19,'https://github.com/ReactiveX/RxJava/wiki/Subject','Subjects');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (19,'https://github.com/ReactiveX/RxJava/wiki/Error-Handling','Error Handling');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (19,'https://github.com/ReactiveX/RxJava/wiki/Alphabetical-List-of-Observable-Operators','Operators');


/*Resilience4j*/

INSERT INTO topic_detail (topic_Id, url, description) VALUES (22,'http://resilience4j.github.io/resilience4j/#_circuitbreaker','CircuitBreaker');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (22,'http://resilience4j.github.io/resilience4j/#_ratelimiter','RateLimiter');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (22,'http://resilience4j.github.io/resilience4j/#_bulkhead','Bulkhead');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (22,'http://resilience4j.github.io/resilience4j/#_retry','Retry');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (22,'http://resilience4j.github.io/resilience4j/#_cache','Cache');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (22,'http://resilience4j.github.io/resilience4j/#_timelimiter','TimeLimiter');

/*Vavr*/

INSERT INTO topic_detail (topic_Id, url, description) VALUES (23,'http://www.vavr.io/vavr-docs/#_tuples','Tuples');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (23,'http://www.vavr.io/vavr-docs/#_functions','Functions');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (23,'http://www.vavr.io/vavr-docs/#_values','Values');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (23,'http://www.vavr.io/vavr-docs/#_collections','Collections');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (23,'http://www.vavr.io/vavr-docs/#_property_checking','Property Checking');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (23,'http://www.vavr.io/vavr-docs/#_pattern_matching','Pattern Matching');


/*Apache POI*/


INSERT INTO topic_detail (topic_Id, url, description) VALUES (20,'https://poi.apache.org/components/spreadsheet/index.html','Excel');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (20,'https://poi.apache.org/components/document/index.html','Word');


/*Neo4j*/

INSERT INTO topic_detail (topic_Id, url, description) VALUES (24,'https://neo4j.com/graphacademy/online-training/introduction-to-neo4j/','Introduction to Graph Databases');
INSERT INTO topic_detail (topic_Id, description) VALUES (24,'Introduction to Neo4j');
INSERT INTO topic_detail (topic_Id, description) VALUES (24,'Setting up your Development Environment');
INSERT INTO topic_detail (topic_Id, description) VALUES (24,'Introduction to Cypher');
INSERT INTO topic_detail (topic_Id, description) VALUES (24,'Getting More out of Queries');
INSERT INTO topic_detail (topic_Id, description) VALUES (24,'Creating Nodes and Relationships');
INSERT INTO topic_detail (topic_Id, description) VALUES (24,'Getting More out of Neo4j');



/*Web*/

/*Servlets and JSP*/
INSERT INTO topic_detail (topic_Id, url, description) VALUES (25,'https://learning.oreilly.com/library/view/head-first-servlets/9780596516680/ch01.html','Intro and Overview');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (25,'https://learning.oreilly.com/library/view/head-first-servlets/9780596516680/ch02.html','Web App Architecture');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (25,'https://learning.oreilly.com/library/view/head-first-servlets/9780596516680/ch03.html','Hands-on MVC');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (25,'https://learning.oreilly.com/library/view/head-first-servlets/9780596516680/ch04.html','Request and Response');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (25,'https://learning.oreilly.com/library/view/head-first-servlets/9780596516680/ch05.html','Attributes and Listeners');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (25,'https://learning.oreilly.com/library/view/head-first-servlets/9780596516680/ch06.html','Session Management');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (25,'https://learning.oreilly.com/library/view/head-first-servlets/9780596516680/ch07.html','Using JSP');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (25,'https://learning.oreilly.com/library/view/head-first-servlets/9780596516680/ch08.html','Scriptless JSP');


/*Spring Framework*/
/*Core*/

INSERT INTO topic_detail (topic_Id, url, description) VALUES (28,'https://docs.spring.io/spring/docs/5.2.0.BUILD-SNAPSHOT/spring-framework-reference/core.html#beans','The IoC Container');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (28,'https://docs.spring.io/spring/docs/5.2.0.BUILD-SNAPSHOT/spring-framework-reference/core.html#resources','Resources');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (28,'https://docs.spring.io/spring/docs/5.2.0.BUILD-SNAPSHOT/spring-framework-reference/core.html#validation','Validation, Data Binding, and Type Conversion');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (28,'https://docs.spring.io/spring/docs/5.2.0.BUILD-SNAPSHOT/spring-framework-reference/core.html#expressions','Spring Expression Language (SpEL)');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (28,'https://docs.spring.io/spring/docs/5.2.0.BUILD-SNAPSHOT/spring-framework-reference/core.html#aop','Aspect Oriented Programming with Spring');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (28,'https://docs.spring.io/spring/docs/5.2.0.BUILD-SNAPSHOT/spring-framework-reference/core.html#aop-api','Spring AOP APIs');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (28,'https://docs.spring.io/spring/docs/5.2.0.BUILD-SNAPSHOT/spring-framework-reference/core.html#null-safety','Null-safety');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (28,'https://docs.spring.io/spring/docs/5.2.0.BUILD-SNAPSHOT/spring-framework-reference/core.html#databuffers','Data Buffers and Codecs');


/*Testing*/

INSERT INTO topic_detail (topic_Id, url, description) VALUES (29,'https://docs.spring.io/spring/docs/5.2.0.BUILD-SNAPSHOT/spring-framework-reference/testing.html#unit-testing','Unit Testing');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (29,'https://docs.spring.io/spring/docs/5.2.0.BUILD-SNAPSHOT/spring-framework-reference/testing.html#integration-testing','Integration Testing');



/*Data Access*/

INSERT INTO topic_detail (topic_Id, url, description) VALUES (30,'https://docs.spring.io/spring/docs/5.2.0.BUILD-SNAPSHOT/spring-framework-reference/data-access.html#transaction','Transaction Management');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (30,'https://docs.spring.io/spring/docs/5.2.0.BUILD-SNAPSHOT/spring-framework-reference/core.html#aop','DAO Support');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (30,'https://docs.spring.io/spring/docs/5.2.0.BUILD-SNAPSHOT/spring-framework-reference/data-access.html#jdbc','Data Access with JDBC');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (30,'https://docs.spring.io/spring/docs/5.2.0.BUILD-SNAPSHOT/spring-framework-reference/data-access.html#orm','Object Relational Mapping (ORM) Data Access');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (30,'https://docs.spring.io/spring/docs/5.2.0.BUILD-SNAPSHOT/spring-framework-reference/data-access.html#oxm','Marshalling XML by Using Object-XML Mappers');



/*Web Servlet*/


INSERT INTO topic_detail (topic_Id, url, description) VALUES (31,'https://docs.spring.io/spring/docs/5.2.0.BUILD-SNAPSHOT/spring-framework-reference/web.html#mvc','Spring Web MVC');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (31,'https://docs.spring.io/spring/docs/5.2.0.BUILD-SNAPSHOT/spring-framework-reference/web.html#webmvc-client','REST Clients');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (31,'https://docs.spring.io/spring/docs/5.2.0.BUILD-SNAPSHOT/spring-framework-reference/web.html#testing','Testing');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (31,'https://docs.spring.io/spring/docs/5.2.0.BUILD-SNAPSHOT/spring-framework-reference/web.html#websocket','WebSockets');


/*Web Reactive*/



INSERT INTO topic_detail (topic_Id, url, description) VALUES (32,'https://docs.spring.io/spring/docs/5.2.0.BUILD-SNAPSHOT/spring-framework-reference/web-reactive.html#webflux','Spring WebFlux');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (32,'https://docs.spring.io/spring/docs/5.2.0.BUILD-SNAPSHOT/spring-framework-reference/web-reactive.html#webflux-client','WebClient');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (32,'https://docs.spring.io/spring/docs/5.2.0.BUILD-SNAPSHOT/spring-framework-reference/web-reactive.html#webflux-websocket','WebSockets');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (32,'https://docs.spring.io/spring/docs/5.2.0.BUILD-SNAPSHOT/spring-framework-reference/web-reactive.html#webflux-test','Testing');


/*Integration*/

INSERT INTO topic_detail (topic_Id, url, description) VALUES (33,'https://docs.spring.io/spring/docs/5.2.0.BUILD-SNAPSHOT/spring-framework-reference/integration.html#remoting','Remoting and Web Services with Spring');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (33,'https://docs.spring.io/spring/docs/5.2.0.BUILD-SNAPSHOT/spring-framework-reference/integration.html#ejb','Enterprise JavaBeans (EJB) Integration');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (33,'https://docs.spring.io/spring/docs/5.2.0.BUILD-SNAPSHOT/spring-framework-reference/integration.html#jms','JMS');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (33,'https://docs.spring.io/spring/docs/5.2.0.BUILD-SNAPSHOT/spring-framework-reference/integration.html#jmx','JMX');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (33,'https://docs.spring.io/spring/docs/5.2.0.BUILD-SNAPSHOT/spring-framework-reference/integration.html#cci','JCA CCI');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (33,'https://docs.spring.io/spring/docs/5.2.0.BUILD-SNAPSHOT/spring-framework-reference/integration.html#mail','Email');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (33,'https://docs.spring.io/spring/docs/5.2.0.BUILD-SNAPSHOT/spring-framework-reference/integration.html#scheduling','Task Execution and Scheduling');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (33,'https://docs.spring.io/spring/docs/5.2.0.BUILD-SNAPSHOT/spring-framework-reference/integration.html#cache','Cache Abstraction');


/*Languages*/

INSERT INTO topic_detail (topic_Id, url, description) VALUES (34,'https://docs.spring.io/spring/docs/5.2.0.BUILD-SNAPSHOT/spring-framework-reference/languages.html#kotlin','Kotlin');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (34,'https://docs.spring.io/spring/docs/5.2.0.BUILD-SNAPSHOT/spring-framework-reference/languages.html#groovy','Apache Groovy');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (34,'https://docs.spring.io/spring/docs/5.2.0.BUILD-SNAPSHOT/spring-framework-reference/languages.html#dynamic-language','Dynamic Language Support');


/*Spring Security*/
/*Java Configuration*/


INSERT INTO topic_detail (topic_Id,description) VALUES (35,'Hello Web Security Java Configuration');
INSERT INTO topic_detail (topic_Id,description) VALUES (35,'HttpSecurity');
INSERT INTO topic_detail (topic_Id,description) VALUES (35,'Java Configuration and Form Login');
INSERT INTO topic_detail (topic_Id,description) VALUES (35,'Authorize Requests');
INSERT INTO topic_detail (topic_Id,description) VALUES (35,'OAuth 2.0 Client');
INSERT INTO topic_detail (topic_Id,description) VALUES (35,'OAuth 2.0 Login');
INSERT INTO topic_detail (topic_Id,description) VALUES (35,'OAuth 2.0 Resource Server');
INSERT INTO topic_detail (topic_Id,description) VALUES (35,'Authentication');
INSERT INTO topic_detail (topic_Id,description) VALUES (35,'Multiple HttpSecurity');
INSERT INTO topic_detail (topic_Id,description) VALUES (35,'Method Security');


/*Security Namespace Configuration*/

INSERT INTO topic_detail (topic_Id,url,description) VALUES (36,'https://docs.spring.io/spring-security/site/docs/5.2.0.BUILD-SNAPSHOT/reference/htmlsingle/#ns-getting-started','Getting Started');
INSERT INTO topic_detail (topic_Id,description) VALUES (36,'Design of the Namespace');
INSERT INTO topic_detail (topic_Id,description) VALUES (36,'Advanced Web Features');
INSERT INTO topic_detail (topic_Id,description) VALUES (36,'Method Security');
INSERT INTO topic_detail (topic_Id,description) VALUES (36,'The Default AccessDecisionManager');
INSERT INTO topic_detail (topic_Id,description) VALUES (36,'The Authentication Manager and the Namespace');


/*Architecture and Implementation*/

INSERT INTO topic_detail (topic_Id,description) VALUES (37,'Technical Overview');
INSERT INTO topic_detail (topic_Id,description) VALUES (37,'Core Services');

/*Testing*/

INSERT INTO topic_detail (topic_Id,description) VALUES (37,'Testing Method Security');
INSERT INTO topic_detail (topic_Id,description) VALUES (37,'Spring MVC Test Integration');


/*Web Application Security */

INSERT INTO topic_detail (topic_Id,description) VALUES (39,'The Security Filter Chain');
INSERT INTO topic_detail (topic_Id,description) VALUES (39,'Core Security Filters');
INSERT INTO topic_detail (topic_Id,description) VALUES (39,'Servlet API integration');
INSERT INTO topic_detail (topic_Id,description) VALUES (39,'Basic and Digest Authentication');
INSERT INTO topic_detail (topic_Id,description) VALUES (39,'Remember-Me Authentication');
INSERT INTO topic_detail (topic_Id,description) VALUES (39,'Cross Site Request Forgery (CSRF)');
INSERT INTO topic_detail (topic_Id,description) VALUES (39,'CORS');
INSERT INTO topic_detail (topic_Id,description) VALUES (39,'Security HTTP Response Headers');
INSERT INTO topic_detail (topic_Id,description) VALUES (39,'Session Management');
INSERT INTO topic_detail (topic_Id,description) VALUES (39,'Anonymous Authentication');
INSERT INTO topic_detail (topic_Id,description) VALUES (39,'WebSocket Security');

/*Authorization*/

INSERT INTO topic_detail (topic_Id,description) VALUES (40,'Authorization Architecture');
INSERT INTO topic_detail (topic_Id,description) VALUES (40,'Secure Object Implementations');
INSERT INTO topic_detail (topic_Id,description) VALUES (40,'Expression-Based Access Control');

/*Reactive Application Security*/



INSERT INTO topic_detail (topic_Id,description) VALUES (41,'WebFlux Security');
INSERT INTO topic_detail (topic_Id,description) VALUES (41,'Default Security Headers');
INSERT INTO topic_detail (topic_Id,description) VALUES (41,'Redirect to HTTPS');
INSERT INTO topic_detail (topic_Id,description) VALUES (41,'OAuth2 WebFlux');
INSERT INTO topic_detail (topic_Id,description) VALUES (41,'WebClient');
INSERT INTO topic_detail (topic_Id,description) VALUES (41,'EnableReactiveMethodSecurity');
INSERT INTO topic_detail (topic_Id,description) VALUES (41,'Reactive Test Support');



/*Testing*/
/*Junit*/

INSERT INTO topic_detail (topic_Id, url, description) VALUES (42,'https://junit.org/junit5/docs/current/user-guide/#writing-tests','Writing Tests');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (42,'https://junit.org/junit5/docs/current/user-guide/#running-tests','Running Tests');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (42,'https://junit.org/junit5/docs/current/user-guide/#extensions','Extension Model');


/*Gatling*/


INSERT INTO topic_detail (topic_Id, url, description) VALUES (47,'https://gatling.io/docs/current/installation','"Installation"');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (47,'https://gatling.io/docs/current/quickstart','Quickstart');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (47,'https://gatling.io/docs/current/advanced_tutorial','Advanced Tutorial');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (47,'https://gatling.io/docs/current/general','"General"');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (47,'https://gatling.io/docs/current/session','"Session"');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (47,'https://gatling.io/docs/current/http','HTTP');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (47,'https://gatling.io/docs/current/jms','JMS');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (47,'https://gatling.io/docs/current/mqtt','MQTT');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (47,'https://gatling.io/docs/current/realtime_monitoring','Realtime monitoring');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (47,'https://gatling.io/docs/current/extensions','Extensions');



/*Mockito*/

INSERT INTO topic_detail (topic_Id, url, description) VALUES (43,'https://static.javadoc.io/org.mockito/mockito-core/2.27.0/org/mockito/Mockito.html','Documentation');

/*Hamcrest*/

INSERT INTO topic_detail (topic_Id, url, description) VALUES (45,'http://hamcrest.org/JavaHamcrest/tutorial','Documentation');

/*AssertJ*/

INSERT INTO topic_detail (topic_Id,description) VALUES (44,'Core assertions guide');
INSERT INTO topic_detail (topic_Id,description) VALUES (44,'Extending assertions');
INSERT INTO topic_detail (topic_Id,description) VALUES (44,'Migrating assertions');



/*WireMock*/


INSERT INTO topic_detail (topic_Id, url, description) VALUES (46,'http://wiremock.org/docs/getting-started/','Getting Started');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (46,'http://wiremock.org/docs/download-and-installation/','Installation');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (46,'http://wiremock.org/docs/junit-rule/','The JUnit 4.x Rule');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (46,'http://wiremock.org/docs/java-usage/','"Java (Non-JUnit) Usage"');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (46,'http://wiremock.org/docs/configuration/','"Configuration"');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (46,'http://wiremock.org/docs/running-standalone/','Running as a Standalone Process');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (46,'http://wiremock.org/docs/stubbing/','Stubbing');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (46,'http://wiremock.org/docs/verifying/','Verifying');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (46,'http://wiremock.org/docs/request-matching/','Request Matching');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (46,'http://wiremock.org/docs/proxying/','Proxying');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (46,'http://wiremock.org/docs/record-playback/','Record and Playback (New)');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (46,'http://wiremock.org/docs/response-templating/','Response Templating');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (46,'http://wiremock.org/docs/record-playback-legacy/','Record and Playback (Legacy)');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (46,'http://wiremock.org/docs/simulating-faults/','"Simulating Faults"');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (46,'http://wiremock.org/docs/stateful-behaviour/','Stateful Behaviour');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (46,'http://wiremock.org/docs/https/','HTTPS');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (46,'http://wiremock.org/docs/extending-wiremock/','Extending WireMock');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (46,'http://wiremock.org/docs/spring-boot/','Spring Boot');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (46,'http://wiremock.org/docs/stub-metadata/','Stub Metadata');



/*Operations*/

/*Maven Basics*/

INSERT INTO topic_detail (topic_Id, url, description) VALUES (72,'https://maven.apache.org/guides/introduction/introduction-to-the-lifecycle.html','The Build Lifecycle');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (72,'https://maven.apache.org/guides/introduction/introduction-to-the-pom.html','The POM');


/*Gradle Basics*/


INSERT INTO topic_detail (topic_Id,description) VALUES (73,'Creating a New Gradle Build');
INSERT INTO topic_detail (topic_Id,description) VALUES (73,'Building Java Libraries');

/*Maven Advanced*/

INSERT INTO topic_detail (topic_Id, url, description) VALUES (74,'https://maven.apache.org/guides/introduction/introduction-to-profiles.html','Profiles');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (74,'https://maven.apache.org/guides/introduction/introduction-to-repositories.html','Repositories');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (74,'https://maven.apache.org/guides/introduction/introduction-to-the-standard-directory-layout.html','Standard Directory Layout');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (74,'https://maven.apache.org/guides/introduction/introduction-to-dependency-mechanism.html','The Dependency Mechanism');


/*Gradle Advanced*/


INSERT INTO topic_detail (topic_Id,description) VALUES (75,'Creating Multi-project Builds');
INSERT INTO topic_detail (topic_Id,description) VALUES (75,'Building Java Applications');
INSERT INTO topic_detail (topic_Id,description) VALUES (75,'Building Java Web Applications');
INSERT INTO topic_detail (topic_Id,description) VALUES (75,'Building Spring Boot 2 Applications with Gradle');
INSERT INTO topic_detail (topic_Id,description) VALUES (75,'Optimizing Gradle Build Performance');
INSERT INTO topic_detail (topic_Id,description) VALUES (75,'Using the Build Cache');


/*Docker*/


INSERT INTO topic_detail (topic_Id, url, description) VALUES (76,'https://docs.docker.com/get-started/part1','Orientation');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (76,'https://docs.docker.com/get-started/part2','Containers');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (76,'https://docs.docker.com/get-started/part3','Services');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (76,'https://docs.docker.com/get-started/part4','Swarms');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (76,'https://docs.docker.com/get-started/part5','Stacks');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (76,'https://docs.docker.com/get-started/part6','Deploy your app');

/*Docker Advanced*/

INSERT INTO topic_detail (topic_Id, url, description) VALUES (79,'https://docs.docker.com/develop/','Develop with Docker');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (79,'https://docs.docker.com/network/','Configure networking');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (79,'https://docs.docker.com/storage/','Manage application data');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (79,'https://docs.docker.com/config/labels-custom-metadata/','Configure all objects');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (79,'https://docs.docker.com/config/daemon/','Configure the daemon');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (79,'https://docs.docker.com/config/thirdparty/','Work with external tools');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (79,'https://docs.docker.com/config/containers/start-containers-automatically/','Configure containers');

/*Version Control*/

/*Git Basics*/

INSERT INTO topic_detail (topic_Id, url, description) VALUES (198,'https://git-scm.com/book/en/v2/Getting-Started-What-is-Git%3F','What is Git');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (198,'https://git-scm.com/book/en/v2/Git-Basics-Getting-a-Git-Repository','Getting a Git Repository');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (198,'https://git-scm.com/book/en/v2/Git-Basics-Recording-Changes-to-the-Repository','Recording Changes to the Repository');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (198,'https://git-scm.com/book/en/v2/Git-Basics-Viewing-the-Commit-History','Viewing the Commit History');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (198,'https://git-scm.com/book/en/v2/Git-Basics-Undoing-Things','Undoing Things');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (198,'https://git-scm.com/book/en/v2/Git-Basics-Working-with-Remotes','Working with Remotes');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (198,'https://git-scm.com/book/en/v2/Git-Basics-Tagging','Tagging');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (198,'https://git-scm.com/book/en/v2/Git-Basics-Git-Aliases','Git Aliases');

/*Git Advanced*/

INSERT INTO topic_detail (topic_Id, url, description) VALUES (199,'"https://git-scm.com/book/en/v2/Git-Branching-Branches-in-a-Nutshell','Branches in a Nutshell');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (199,'https://git-scm.com/book/en/v2/Git-Branching-Basic-Branching-and-Merging','Basic Branching and Merging');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (199,'https://git-scm.com/book/en/v2/Git-Branching-Branch-Management','Branch Management');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (199,'https://git-scm.com/book/en/v2/Git-Branching-Branching-Workflows','Branching Workflows');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (199,'https://git-scm.com/book/en/v2/Git-Branching-Remote-Branches','Remote Branches');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (199,'https://git-scm.com/book/en/v2/Git-Branching-Rebasing','Rebasing');


/*HTTP & REST API*/


/*HTTP Basics*/

INSERT INTO topic_detail (topic_Id,description) VALUES (89,'HTTP - Overview');
INSERT INTO topic_detail (topic_Id,description) VALUES (89,'HTTP - Parameters');
INSERT INTO topic_detail (topic_Id,description) VALUES (89,'HTTP - Messages');
INSERT INTO topic_detail (topic_Id,description) VALUES (89,'HTTP - Requests');
INSERT INTO topic_detail (topic_Id,description) VALUES (89,'HTTP - Responses');
INSERT INTO topic_detail (topic_Id,description) VALUES (89,'HTTP - Methods');
INSERT INTO topic_detail (topic_Id,description) VALUES (89,'HTTP - Status Codes');


/*HTTP Advanced*/


INSERT INTO topic_detail (topic_Id, url, description) VALUES (90,'https://www.tutorialspoint.com/http/http_header_fields.htm','HTTP - Header Fields');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (90,'https://www.tutorialspoint.com/http/http_caching.htm','HTTP - Caching');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (90,'https://www.tutorialspoint.com/http/http_url_encoding.htm','HTTP - URL Encoding');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (90,'https://www.tutorialspoint.com/http/http_security.htm','HTTP - Security');




/*REST API Basics*/

INSERT INTO topic_detail (topic_Id,description) VALUES (91,'REST API - Introducing');
INSERT INTO topic_detail (topic_Id,description) VALUES (91,'REST API - The Need for REST');
INSERT INTO topic_detail (topic_Id,description) VALUES (91,'REST API - Restful Web API');
INSERT INTO topic_detail (topic_Id,description) VALUES (91,'REST API - Comparison of APIs');
INSERT INTO topic_detail (topic_Id,description) VALUES (91,'REST API - An Intuitive understanding of REST');
INSERT INTO topic_detail (topic_Id,description) VALUES (91,'REST API - REST Constraints');
INSERT INTO topic_detail (topic_Id,description) VALUES (91,'REST API - Concept of Serialization');
INSERT INTO topic_detail (topic_Id,description) VALUES (91,'REST API - The Concept of Deserialization');
INSERT INTO topic_detail (topic_Id,description) VALUES (91,'REST API - Richardson Maturity Model');

/*REST API Advanced*/

INSERT INTO topic_detail (topic_Id,description) VALUES (92,'REST API - Creating Domain Models');
INSERT INTO topic_detail (topic_Id,description) VALUES (92,'REST API - Scaffolding Controllers');
INSERT INTO topic_detail (topic_Id,description) VALUES (92,'REST API - Database Seeding');
INSERT INTO topic_detail (topic_Id,description) VALUES (92,'REST API - Migrations');
INSERT INTO topic_detail (topic_Id,description) VALUES (92,'REST API - Using DTOs');
INSERT INTO topic_detail (topic_Id,description) VALUES (92,'REST API - Implementing Paging');
INSERT INTO topic_detail (topic_Id,description) VALUES (92,'REST API - CORS and Enabling CORS');
INSERT INTO topic_detail (topic_Id,description) VALUES (92,'REST API - Deferred Execution');


/*Design RESTful API*/


INSERT INTO topic_detail (topic_Id, url, description) VALUES (93,'https://learning.oreilly.com/library/view/restful-web-services/9780596809140/ch01.html','Using the Uniform Interface');
INSERT INTO topic_detail (topic_Id,description) VALUES (93,'Identifying Resources');
INSERT INTO topic_detail (topic_Id,description) VALUES (93,'Designing Representations');
INSERT INTO topic_detail (topic_Id,description) VALUES (93,'Designing URIs');
INSERT INTO topic_detail (topic_Id,description) VALUES (93,'Web Linking');
INSERT INTO topic_detail (topic_Id,description) VALUES (93,'Atom and AtomPub');
INSERT INTO topic_detail (topic_Id,description) VALUES (93,'Content Negotiation');
INSERT INTO topic_detail (topic_Id,description) VALUES (93,'Queries');
INSERT INTO topic_detail (topic_Id,description) VALUES (93,'Web Caching');
INSERT INTO topic_detail (topic_Id,description) VALUES (93,'Conditional Requests');
INSERT INTO topic_detail (topic_Id,description) VALUES (93,'Miscellaneous Writes');
INSERT INTO topic_detail (topic_Id,description) VALUES (93,'Security');
INSERT INTO topic_detail (topic_Id,description) VALUES (93,'Extensibility and Versioning');
INSERT INTO topic_detail (topic_Id,description) VALUES (93,'Enabling Discovery');



/*Network*/

/*Basics*/
INSERT INTO topic_detail (topic_Id,description) VALUES (94,'DCN - Overview');
INSERT INTO topic_detail (topic_Id,description) VALUES (94,'DCN - Computer Network Types');
INSERT INTO topic_detail (topic_Id,description) VALUES (94,'DCN - Network LAN Technologies');
INSERT INTO topic_detail (topic_Id,description) VALUES (94,'DCN - Computer Network Topologies');
INSERT INTO topic_detail (topic_Id,description) VALUES (94,'DCN - Computer Network Models');
INSERT INTO topic_detail (topic_Id,description) VALUES (94,'DCN - Computer Network Security');



/*OSI Model*/

INSERT INTO topic_detail (topic_Id,description) VALUES (95,'DCN - Application Layer Introduction');
INSERT INTO topic_detail (topic_Id,description) VALUES (95,'DCN - Client-Server Model');
INSERT INTO topic_detail (topic_Id,description) VALUES (95,'DCN - Application Protocols');
INSERT INTO topic_detail (topic_Id,description) VALUES (95,'DCN - Network Services');


/*IP Addressing*/


INSERT INTO topic_detail (topic_Id,description) VALUES (96,'IPv4 - Overview');
INSERT INTO topic_detail (topic_Id,description) VALUES (96,'IPv4 - OSI Model');
INSERT INTO topic_detail (topic_Id,description) VALUES (96,'IPv4 - TCP/IP Model');
INSERT INTO topic_detail (topic_Id,description) VALUES (96,'IPv4 - Packet Structure');
INSERT INTO topic_detail (topic_Id,description) VALUES (96,'IPv4 - Addressing');
INSERT INTO topic_detail (topic_Id,description) VALUES (96,'IPv4 - Address Classes');
INSERT INTO topic_detail (topic_Id,description) VALUES (96,'IPv4 - Subnetting');
INSERT INTO topic_detail (topic_Id,description) VALUES (96,'IPv4 - VLSM');
INSERT INTO topic_detail (topic_Id,description) VALUES (96,'IPv4 - Reserved Addresses');
INSERT INTO topic_detail (topic_Id,description) VALUES (96,'IPv6 - Overview');
INSERT INTO topic_detail (topic_Id,description) VALUES (96,'IPv6 - Features');
INSERT INTO topic_detail (topic_Id,description) VALUES (96,'IPv6 - Addressing Modes');
INSERT INTO topic_detail (topic_Id,description) VALUES (96,'IPv6 - Address Types');
INSERT INTO topic_detail (topic_Id,description) VALUES (96,'IPv6 - Special Addresses');
INSERT INTO topic_detail (topic_Id,description) VALUES (96,'IPv6 - Headers');
INSERT INTO topic_detail (topic_Id,description) VALUES (96,'IPv6 - Communication');
INSERT INTO topic_detail (topic_Id,description) VALUES (96,'IPv6 - Subnetting');
INSERT INTO topic_detail (topic_Id,description) VALUES (96,'IPv6 - IPv4 to IPv6');
INSERT INTO topic_detail (topic_Id,description) VALUES (96,'IPv6 - Mobility');
INSERT INTO topic_detail (topic_Id,description) VALUES (96,'IPv6 - Routing');



/*IP Subnetting*/


INSERT INTO topic_detail (topic_Id, url, description) VALUES (97,'https://www.techopedia.com/6/28587/internet/8-steps-to-understanding-ip-subnetting/2','Why We Need Subnets');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (97,'https://www.techopedia.com/6/28587/internet/8-steps-to-understanding-ip-subnetting/3','Understanding Binary Numbers');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (97,'https://www.techopedia.com/6/28587/internet/8-steps-to-understanding-ip-subnetting/4','IP Addresses');
INSERT INTO topic_detail (topic_Id,description) VALUES (97,'Subnetting and the Subnet Mask');
INSERT INTO topic_detail (topic_Id,description) VALUES (97,'Public Vs. Private IP Addresses');
INSERT INTO topic_detail (topic_Id,description) VALUES (97,'CIDR IP Addressing');
INSERT INTO topic_detail (topic_Id,description) VALUES (97,'Variable Length Subnet Masking');
INSERT INTO topic_detail (topic_Id,description) VALUES (97,'IPv6 to the Rescue');




/*Basics*/

INSERT INTO topic_detail (topic_Id,description) VALUES (98,'DBMS - Overview');
INSERT INTO topic_detail (topic_Id,description) VALUES (98,'DBMS - Architecture');
INSERT INTO topic_detail (topic_Id,description) VALUES (98,'DBMS - Data Models');
INSERT INTO topic_detail (topic_Id,description) VALUES (98,'DBMS - Data Schemas');
INSERT INTO topic_detail (topic_Id,description) VALUES (98,'DBMS - Data Independence');



/*Entity Relationship Model*/

INSERT INTO topic_detail (topic_Id,description) VALUES (99,'DBMS - ER Model Basic Concepts');
INSERT INTO topic_detail (topic_Id,description) VALUES (99,'DBMS - ER Diagram Representation');
INSERT INTO topic_detail (topic_Id,description) VALUES (99,'DBMS - Generalization, Aggregation');

/*Relational Model*/

INSERT INTO topic_detail (topic_Id,description) VALUES (100,'DBMS - Codd''s Rules');
INSERT INTO topic_detail (topic_Id,description) VALUES (100,'DBMS - Relational Data Model');
INSERT INTO topic_detail (topic_Id,description) VALUES (100,'DBMS - Relational Algebra');
INSERT INTO topic_detail (topic_Id,description) VALUES (100,'DBMS - ER to Relational Model');
INSERT INTO topic_detail (topic_Id,description) VALUES (100,'DBMS- SQL Overview');


/*Relational Database Design*/


INSERT INTO topic_detail (topic_Id,description) VALUES (101,'DBMS - Database Normalization');
INSERT INTO topic_detail (topic_Id,description) VALUES (101,'DBMS - Database Joins');


/*Storage and File Structure*/

INSERT INTO topic_detail (topic_Id,description) VALUES (102,'DBMS - Storage System');
INSERT INTO topic_detail (topic_Id,description) VALUES (102,'DBMS - File Structure');

/*Indexing and Hashing*/

INSERT INTO topic_detail (topic_Id,description) VALUES (103,'DBMS - Indexing');
INSERT INTO topic_detail (topic_Id,description) VALUES (103,'DBMS - Hashing');


/*Transaction And Concurrency*/

INSERT INTO topic_detail (topic_Id,description) VALUES (104,'DBMS - Transaction');
INSERT INTO topic_detail (topic_Id,description) VALUES (104,'DBMS - Concurrency Control');
INSERT INTO topic_detail (topic_Id,description) VALUES (104,'DBMS - Deadlock');

/*Backup and Recovery*/

INSERT INTO topic_detail (topic_Id,description) VALUES (105,'DBMS - Data Backup');
INSERT INTO topic_detail (topic_Id,description) VALUES (105,'DBMS - Data Recovery');

/*Algorithms Basics*/


INSERT INTO topic_detail (topic_Id,description) VALUES (106,'DSA - Algorithms Basics');
INSERT INTO topic_detail (topic_Id,description) VALUES (106,'DSA - Asymptotic Analysis');
INSERT INTO topic_detail (topic_Id,description) VALUES (106,'DSA - Greedy Algorithms');
INSERT INTO topic_detail (topic_Id,description) VALUES (106,'DSA - Divide and Conquer');
INSERT INTO topic_detail (topic_Id,description) VALUES (106,'DSA - Dynamic Programming');

/*Data Structure Basics*/

INSERT INTO topic_detail (topic_Id,description) VALUES (107,'DSA - Data Structure Basics');
INSERT INTO topic_detail (topic_Id,description) VALUES (107,'DSA - Array Data Structure');
INSERT INTO topic_detail (topic_Id,description) VALUES (107,'DSA - Linked List Basics');
INSERT INTO topic_detail (topic_Id,description) VALUES (107,'DSA - Doubly Linked List');

/*Data Structure Advanced*/

INSERT INTO topic_detail (topic_Id,description) VALUES (108,'DSA - Circular Linked List');
INSERT INTO topic_detail (topic_Id,description) VALUES (108,'DSA - Stack');
INSERT INTO topic_detail (topic_Id,description) VALUES (108,'DSA - Expression Parsing');
INSERT INTO topic_detail (topic_Id,description) VALUES (108,'DSA - Queue');

/*Search Algorithms*/

INSERT INTO topic_detail (topic_Id,description) VALUES (109,'DSA - Linear Search');
INSERT INTO topic_detail (topic_Id,description) VALUES (109,'DSA - Binary Search');
INSERT INTO topic_detail (topic_Id,description) VALUES (109,'DSA - Interpolation Search');
INSERT INTO topic_detail (topic_Id,description) VALUES (109,'DSA - Hash Table');

/*Sorting Algorithms*/

INSERT INTO topic_detail (topic_Id,description) VALUES (110,'DSA - Sorting Algorithms');
INSERT INTO topic_detail (topic_Id,description) VALUES (110,'DSA - Bubble Sort');
INSERT INTO topic_detail (topic_Id,description) VALUES (110,'DSA - Insertion Sort');
INSERT INTO topic_detail (topic_Id,description) VALUES (110,'DSA - Selection Sort');
INSERT INTO topic_detail (topic_Id,description) VALUES (110,'DSA - Quick Sort');

/*Graph*/
INSERT INTO topic_detail (topic_Id, url, description) VALUES (111,'https://www.tutorialspoint.com/data_structures_algorithms/graph_data_structure.htm','DSA - Graph Data Structure');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (111,'https://www.tutorialspoint.com/data_structures_algorithms/depth_first_traversal.htm','DSA - Depth First Traversal');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (111,'https://www.tutorialspoint.com/data_structures_algorithms/breadth_first_traversal.htm','DSA - Breadth First Traversal');

/*Tree*/
INSERT INTO topic_detail (topic_Id,description) VALUES (112,'DSA - Tree Data Structure');
INSERT INTO topic_detail (topic_Id,description) VALUES (112,'DSA - Tree Traversal');
INSERT INTO topic_detail (topic_Id,description) VALUES (112,'DSA - Binary Search Tree');
INSERT INTO topic_detail (topic_Id,description) VALUES (112,'DSA - AVL Tree');
INSERT INTO topic_detail (topic_Id,description) VALUES (112,'DSA - Spanning Tree');
INSERT INTO topic_detail (topic_Id,description) VALUES (112,'DSA - Heap');


/*Algorithms Advanced*/


INSERT INTO topic_detail (topic_Id, url, description) VALUES (113,'https://www.tutorialspoint.com/data_structures_algorithms/recursion_basics.htm','DSA - Recursion Basics');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (113,'https://www.tutorialspoint.com/data_structures_algorithms/tower_of_hanoi.htm','DSA - Tower of Hanoi');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (113,'https://www.tutorialspoint.com/data_structures_algorithms/fibonacci_series.htm','DSA - Fibonacci Series');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (113,'https://www.tutorialspoint.com/data_structures_algorithms/shell_sort_algorithm.htm','DSA - Shell Sort');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (113,'https://www.tutorialspoint.com/data_structures_algorithms/merge_sort_algorithm.htm','DSA - Merge Sort');


/*Object-Oriented Paradigm*/

INSERT INTO topic_detail (topic_Id, url, description) VALUES (114,'https://www.tutorialspoint.com/software_architecture_design/object_oriented_paradigm.htm','Object-Oriented Paradigm');


/*Key Principles*/

INSERT INTO topic_detail (topic_Id, url, description) VALUES (115,'https://www.tutorialspoint.com/software_architecture_design/key_principles.htm','Key Principles');

/*Architecture Models*/

INSERT INTO topic_detail (topic_Id, url, description) VALUES (116,'https://www.tutorialspoint.com/software_architecture_design/architecture_models.htm','Architecture Models');

/*Data Flow Architecture*/

INSERT INTO topic_detail (topic_Id, url, description) VALUES (117,'https://www.tutorialspoint.com/software_architecture_design/data_flow_architecture.htm','Data Flow Architecture');

/*Data-Centered Architecture*/

INSERT INTO topic_detail (topic_Id, url, description) VALUES (118,'https://www.tutorialspoint.com/software_architecture_design/data_centered_architecture.htm','Data-Centered Architecture');

/*Hierarchical Architecture*/

INSERT INTO topic_detail (topic_Id, url, description) VALUES (119,'https://www.tutorialspoint.com/software_architecture_design/hierarchical_architecture.htm','Hierarchical Architecture');

/*Interaction-Oriented Architecture*/

INSERT INTO topic_detail (topic_Id, url, description) VALUES (120,'https://www.tutorialspoint.com/software_architecture_design/interaction_oriented_architecture.htm','Interaction-Oriented Architecture');

/*Component-Based Architecture*/

INSERT INTO topic_detail (topic_Id, url, description) VALUES (121,'https://www.tutorialspoint.com/software_architecture_design/component_based_architecture.htm','Component-Based Architecture');


/*Distributed Architecture*/

INSERT INTO topic_detail (topic_Id, url, description) VALUES (122,'https://www.tutorialspoint.com/software_architecture_design/distributed_architecture.htm','Distributed Architecture');



/*User Interface*/

INSERT INTO topic_detail (topic_Id, url, description) VALUES (123,'https://www.tutorialspoint.com/software_architecture_design/user_interface.htm','User Interface');


/*DEsign PAtterns*/

INSERT INTO topic_detail (topic_Id, url, description) VALUES (125,'https://learning.oreilly.com/library/view/head-first-design/0596007124/ch01.html','Intro to Design Patterns');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (126,'https://learning.oreilly.com/library/view/head-first-design/0596007124/ch02.html','The Observer Pattern');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (127,'https://learning.oreilly.com/library/view/head-first-design/0596007124/ch03.html','The Decorator Pattern');

INSERT INTO topic_detail (topic_Id, url, description) VALUES (128,'https://learning.oreilly.com/library/view/head-first-design/0596007124/ch04.html','The Factory Pattern');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (129,'https://learning.oreilly.com/library/view/head-first-design/0596007124/ch05.html','The Singleton Pattern');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (130,'https://learning.oreilly.com/library/view/head-first-design/0596007124/apa.html#bridge','The Bridge Pattern');

INSERT INTO topic_detail (topic_Id, url, description) VALUES (131,'https://learning.oreilly.com/library/view/head-first-design/0596007124/apa.html#flyweight','The Flyweight Pattern');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (132,'https://learning.oreilly.com/library/view/head-first-design/0596007124/apa.html#builder','The Builder Pattern');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (133,'https://learning.oreilly.com/library/view/head-first-design/0596007124/apa.html#interpreter','The Interpreter Pattern');

INSERT INTO topic_detail (topic_Id, url, description) VALUES (134,'"https://learning.oreilly.com/library/view/head-first-design/0596007124/apa.html#mediator','The Mediator Pattern');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (135,'https://learning.oreilly.com/library/view/head-first-design/0596007124/apa.html#memento','The Memento Pattern');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (136,'https://learning.oreilly.com/library/view/head-first-design/0596007124/apa.html#prototype','The Prototype Pattern');

INSERT INTO topic_detail (topic_Id, url, description) VALUES (137,'https://learning.oreilly.com/library/view/head-first-design/0596007124/apa.html#visitor','The Visitor Pattern');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (138,'https://learning.oreilly.com/library/view/head-first-design/0596007124/ch06.html','The Command Pattern');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (139,'https://learning.oreilly.com/library/view/head-first-design/0596007124/ch07.html','The Adapter and Facade Patterns');

INSERT INTO topic_detail (topic_Id, url, description) VALUES (140,'https://learning.oreilly.com/library/view/head-first-design/0596007124/ch08.html','The Template Method Pattern');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (141,'https://learning.oreilly.com/library/view/head-first-design/0596007124/ch09.html','The Iterator and Composite Patterns');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (142,'https://learning.oreilly.com/library/view/head-first-design/0596007124/ch10.html','The State Pattern');

INSERT INTO topic_detail (topic_Id, url, description) VALUES (143,'https://learning.oreilly.com/library/view/head-first-design/0596007124/ch11.html','The Proxy Pattern');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (144,'https://learning.oreilly.com/library/view/head-first-design/0596007124/ch12.html','Compound Patterns');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (145,'https://learning.oreilly.com/library/view/head-first-design/0596007124/ch13.html','Patterns in the Real World');

/*Caching*/

INSERT INTO topic_detail (topic_Id, url, description) VALUES (48,'"https://static.javadoc.io/javax.cache/cache-api/1.0.0/javax/cache/package-summary.html','Java Cache API');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (50,'https://commons.apache.org/proper/commons-jcs/getting_started/intro.html','Apache Commons JCS');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (49,'http://www.ehcache.org/documentation/3.7/','Ehcache');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (51,'https://cache2k.org/','Cache2k');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (52,'https://github.com/google/guava/wiki/CachesExplained','Guava Caches');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (53,'https://docs.spring.io/spring/docs/5.2.0.BUILD-SNAPSHOT/spring-framework-reference/integration.html#cache','Spring Cache');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (54,'https://redis.io/topics/introduction','Redis Cache');


/*Messaging*/


INSERT INTO topic_detail (topic_Id, url, description) VALUES (55,'https://docs.oracle.com/javaee/6/tutorial/doc/bncdq.html','Java Message Service API');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (57,'http://activemq.apache.org/getting-started','ActiveMQ');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (56,'https://www.rabbitmq.com/getstarted.html','RabbitMQ');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (58,'https://redis.io/documentation','Redis');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (59,'https://kafka.apache.org/intro','Kafka');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (60,'http://rocketmq.apache.org/docs/quick-start/','RocketMQ');


/*Persistense*/

/*JDBC*/


INSERT INTO topic_detail (topic_Id, url, description) VALUES (61,'https://www.javatpoint.com/jdbc-tutorial','JDBC Introduction');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (61,'https://www.javatpoint.com/jdbc-driver','JDBC Driver');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (61,'"https://www.javatpoint.com/steps-to-connect-to-the-database-in-java','DB Connectivity Steps');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (61,'https://www.javatpoint.com/example-to-connect-to-the-oracle-database','Connectivity with Oracle');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (61,'https://www.javatpoint.com/example-to-connect-to-the-mysql-database','Connectivity with MySQL');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (61,'https://www.javatpoint.com/connectivity-with-access-without-dsn','Access without DSN');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (61,'https://www.javatpoint.com/DriverManager-class','DriverManager');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (61,'https://www.javatpoint.com/Connection-interface','Connection');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (61,'https://www.javatpoint.com/Statement-interface','Statement');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (61,'https://www.javatpoint.com/ResultSet-interface','ResultSet');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (61,'https://www.javatpoint.com/PreparedStatement-interface','PreparedStatement');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (61,'https://www.javatpoint.com/ResultSetMetaData-interface','ResultSetMetaData');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (61,'https://www.javatpoint.com/DatabaseMetaData-interface','DatabaseMetaData');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (61,'https://www.javatpoint.com/storing-image-in-oracle-database','Store image');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (61,'https://www.javatpoint.com/retrieving-image-from-oracle-database','Retrieve image');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (61,'https://www.javatpoint.com/storing-file-in-oracle-database','Store file');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (61,'https://www.javatpoint.com/retrieving-file-from-oracle-database','Retrieve file');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (61,'https://www.javatpoint.com/CallableStatement-interface','CallableStatement');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (61,'https://www.javatpoint.com/transaction-management-in-jdbc','Transaction Management');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (61,'https://www.javatpoint.com/batch-processing-in-jdbc','Batch Processing');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (61,'https://www.javatpoint.com/jdbc-rowset','RowSet Interface');

/*Java Persistence API*/

INSERT INTO topic_detail (topic_Id, url, description) VALUES (62,'https://docs.oracle.com/javaee/6/tutorial/doc/bnbpz.html','Java Persistence API');

/*Java Data Objects API*/
INSERT INTO topic_detail (topic_Id, url, description) VALUES (63,'http://db.apache.org/jdo/index.html','Java Data Objects API');


/*Hibernate*/


INSERT INTO topic_detail (topic_Id, url, description) VALUES (64,'http://docs.jboss.org/hibernate/orm/5.4/quickstart/html_single/','Documentation');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (64,'https://www.javatpoint.com/hibernate-architecture','Hibernate Architecture');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (64,'https://www.javatpoint.com/hibernate-inheritance-mapping-tutorial','Inheritance Mapping');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (64,'https://www.javatpoint.com/collection-mapping','Collection Mapping');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (64,'https://www.javatpoint.com/hibernate-transaction-management-example','Transaction Management');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (64,'https://www.javatpoint.com/hql','Hibernate Query Language');

/*ElasticSearch*/

INSERT INTO topic_detail (topic_Id, url, description) VALUES (65,'https://www.elastic.co/guide/en/elasticsearch/reference/current/index.html','Documentation');

/*Apache OpenJPA*/

INSERT INTO topic_detail (topic_Id, url, description) VALUES (67,'http://openjpa.apache.org/builds/3.0.0/apache-openjpa/docs/manual.html','Documentation');

/*EclipseLink*/

INSERT INTO topic_detail (topic_Id, url, description) VALUES (66,'https://www.eclipse.org/eclipselink/documentation/2.7/concepts/toc.htm','Documentation');

/*ObjectDB*/

INSERT INTO topic_detail (topic_Id, url, description) VALUES (68,'https://www.objectdb.com/api/java/jpa/Persistence','JPA');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (68,'https://www.objectdb.com/api/java/jdo/JDOHelper','JDO');

/*Datanucleus*/

INSERT INTO topic_detail (topic_Id, url, description) VALUES (71,'http://www.datanucleus.org/products/accessplatform/','Documentation');



/*Front-end*/


/*JavaScript*/


/*The JavaScript language*/

INSERT INTO topic_detail (topic_Id,description) VALUES (146,'An introduction');
INSERT INTO topic_detail (topic_Id,description) VALUES (146,'JavaScript Fundamentals');
INSERT INTO topic_detail (topic_Id,description) VALUES (146,'Code quality');
INSERT INTO topic_detail (topic_Id,description) VALUES (146,'Objects: the basics');
INSERT INTO topic_detail (topic_Id,description) VALUES (146,'Data types');
INSERT INTO topic_detail (topic_Id,description) VALUES (146,'Advanced working with functions');
INSERT INTO topic_detail (topic_Id,description) VALUES (146,'Objects, classes, inheritance');
INSERT INTO topic_detail (topic_Id,description) VALUES (146,'Error handling');

/*Browser: Document, Events, Interfaces*/

INSERT INTO topic_detail (topic_Id, url, description) VALUES (147,'http://javascript.info/document','Document');
INSERT INTO topic_detail (topic_Id,description) VALUES (147,'Introduction into Events');
INSERT INTO topic_detail (topic_Id,description) VALUES (147,'Events in details');
INSERT INTO topic_detail (topic_Id,description) VALUES (147,'Forms, controls');

/*Frames and windows*/

INSERT INTO topic_detail (topic_Id, url, description) VALUES (148,'http://javascript.info/popup-windows','Popups and window methods');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (148,'http://javascript.info/cross-window-communication','Cross-window communication');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (148,'http://javascript.info/clickjacking','The clickjacking attack');


/*Promises, async/await*/


INSERT INTO topic_detail (topic_Id, url, description) VALUES (149,'http://javascript.info/callbacks','Introduction: callbacks');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (149,'http://javascript.info/promise-basics','Promise');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (149,'http://javascript.info/promise-chaining','Promises chaining');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (149,'http://javascript.info/promise-api','Promise API');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (149,'http://javascript.info/async-await','Async/await');

/*Network requests*/
INSERT INTO topic_detail (topic_Id,description) VALUES (150,'XMLHttpRequest and AJAX');

/*Animation*/

INSERT INTO topic_detail (topic_Id, url, description) VALUES (151,'http://javascript.info/js-animation','JavaScript animations');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (151,'http://javascript.info/css-animations','CSS-animations');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (151,'http://javascript.info/bezier-curve','Bezier curve');

/*Regular expressions*/

INSERT INTO topic_detail (topic_Id, url, description) VALUES (152,'http://javascript.info/regexp-introduction','Patterns and flags');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (152,'http://javascript.info/regexp-methods','Methods of RegExp and String');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (152,'http://javascript.info/regexp-escaping','Escaping, special characters');



/*TypeScript*/

/*The basic of Typescript*/


INSERT INTO topic_detail (topic_Id, url, description) VALUES (153,'https://www.typescriptlang.org/docs/handbook/basic-types.html','Basic Types');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (153,'https://www.typescriptlang.org/docs/handbook/variable-declarations.html','Variable Declarations');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (153,'https://www.typescriptlang.org/docs/handbook/interfaces.html','Interfaces');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (153,'https://www.typescriptlang.org/docs/handbook/classes.html','Classes');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (153,'https://www.typescriptlang.org/docs/handbook/functions.html','Functions');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (153,'https://www.typescriptlang.org/docs/handbook/generics.html','Generics');


/*Typescript modules*/
INSERT INTO topic_detail (topic_Id, description) VALUES (154,'Modules');
INSERT INTO topic_detail (topic_Id, description) VALUES (154,'Namespaces');
INSERT INTO topic_detail (topic_Id, description) VALUES (154,'Namespaces and Modules');
INSERT INTO topic_detail (topic_Id, description) VALUES (154,'Module Resolution');


/*Declaration Files*/

INSERT INTO topic_detail (topic_Id, description) VALUES (155,'Introduction');
INSERT INTO topic_detail (topic_Id, description) VALUES (155,'Library Structures');
INSERT INTO topic_detail (topic_Id, description) VALUES (155,'By Example');

/*Project Configuration*/

INSERT INTO topic_detail (topic_Id, description) VALUES (156,'tsconfig.json');
INSERT INTO topic_detail (topic_Id, description) VALUES (156,'Compiler Options');
INSERT INTO topic_detail (topic_Id, description) VALUES (156,'Project References');

/*HTml Css*/

/*Basics*/

INSERT INTO topic_detail (topic_Id, url, description) VALUES (157,'https://learn.shayhowe.com/html-css/building-your-first-web-page/','Introduction');
INSERT INTO topic_detail (topic_Id,  description) VALUES (157,'Getting to Know HTML');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (157,'https://learn.shayhowe.com/html-css/getting-to-know-css/','Getting to Know CSS');

/*Basics Part 2*/
INSERT INTO topic_detail (topic_Id, description) VALUES (158,'Setting Backgrounds & Gradients');
INSERT INTO topic_detail (topic_Id, description) VALUES (158,'Creating Lists');
INSERT INTO topic_detail (topic_Id, description) VALUES (158,'Adding Media');

/*Advanced*/

INSERT INTO topic_detail (topic_Id, url, description) VALUES (159,'https://learn.shayhowe.com/advanced-html-css/performance-organization/','Performance & Organization');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (159,'https://learn.shayhowe.com/advanced-html-css/detailed-css-positioning/','Detailed Positioning');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (159,'https://learn.shayhowe.com/advanced-html-css/complex-selectors/','Complex Selectors');

/*Advanced Part 2*/

INSERT INTO topic_detail (topic_Id, description) VALUES (160,'Transforms');
INSERT INTO topic_detail (topic_Id, description) VALUES (160,'Transitions & Animations');
INSERT INTO topic_detail (topic_Id, description) VALUES (160,'Feature Support & Polyfills');

/*CSS Architecture*/
INSERT INTO topic_detail (topic_Id, url, description) VALUES (161,'https://zendev.com/ultimate-guide-to-learning-css.html#css-naming-systems-and-architecture','CSS Architecture');

/*Frameworks & Libraries*/

/*jQuery Basics*/

INSERT INTO topic_detail (topic_Id, url, description) VALUES (162,'https://learn.shayhowe.com/advanced-html-css/jquery/#jquery','jQuery Intro');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (162,'https://learn.shayhowe.com/advanced-html-css/jquery/#selectors','Selectors');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (162,'https://learn.shayhowe.com/advanced-html-css/jquery/#traversing','Traversing');

/*Angular Basics*/
INSERT INTO topic_detail (topic_Id, url, description) VALUES (163,'https://angular.io/guide/architecture','Architecture Overview');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (163,'https://angular.io/guide/architecture-modules','Intro to Modules');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (163,'https://angular.io/guide/architecture-components','Intro to Components');

/*Angular Material Advanced*/

INSERT INTO topic_detail (topic_Id, description) VALUES (166,'Introduction');
INSERT INTO topic_detail (topic_Id, description) VALUES (166,'Transition and Triggers');
INSERT INTO topic_detail (topic_Id, description) VALUES (166,'Complex Sequences');

/*WebSockets*/
INSERT INTO topic_detail (topic_Id, description) VALUES (172,'WebSockets – Overview');
INSERT INTO topic_detail (topic_Id, description) VALUES (172,'WebSockets - Duplex Communication');
INSERT INTO topic_detail (topic_Id, description) VALUES (172,'WebSockets – Functionalities');

/*Socket IO*/
INSERT INTO topic_detail (topic_Id, description) VALUES (173,'Socket.IO - Overview');
INSERT INTO topic_detail (topic_Id, description) VALUES (173,'Socket.IO - Environment');
INSERT INTO topic_detail (topic_Id, description) VALUES (173,'Socket.IO - Hello World');

/*SockJS*/

INSERT INTO topic_detail (topic_Id, url, description) VALUES (174,'https://github.com/sockjs','Documentation');

/*BPN NodeJS*/
/*Introduction to packages and modules*/

INSERT INTO topic_detail (topic_Id, url, description) VALUES (175,'https://docs.npmjs.com/about-the-public-npm-registry','About the public npm registry');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (175,'https://docs.npmjs.com/about-packages-and-modules','About packages and modules');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (175,'https://docs.npmjs.com/about-scopes','About scopes');

/*Contributing packages to the registry*/

INSERT INTO topic_detail (topic_Id, description) VALUES (176,'Creating a package.json file');
INSERT INTO topic_detail (topic_Id, description) VALUES (176,'Creating Node.js modules');
INSERT INTO topic_detail (topic_Id, description) VALUES (176,'About package README files');

/*Updating and managing packages*/

INSERT INTO topic_detail (topic_Id, description) VALUES (177,'Changing package visibility');
INSERT INTO topic_detail (topic_Id, description) VALUES (177,'Adding collaborators to private packages');
INSERT INTO topic_detail (topic_Id, description) VALUES (177,'Updating your published package version number');

/*NodeJS Basics*/

INSERT INTO topic_detail (topic_Id, description) VALUES (178,'Node.js - Introduction');
INSERT INTO topic_detail (topic_Id, description) VALUES (178,'Node.js - Environment Setup');
INSERT INTO topic_detail (topic_Id, description) VALUES (178,'Node.js - First Application');

/*NodeJS Advanced*/
INSERT INTO topic_detail (topic_Id, description) VALUES (179,'Node.js - File System');
INSERT INTO topic_detail (topic_Id, description) VALUES (179,'Node.js - Global Objects');
INSERT INTO topic_detail (topic_Id, description) VALUES (179,'Node.js - Utility Modules');


/*Authentication*/

/*OAuth2.0*/
INSERT INTO topic_detail (topic_Id, description) VALUES (180,'OAuth 2.0 - Overview');
INSERT INTO topic_detail (topic_Id, description) VALUES (180,'OAuth 2.0 - Architecture');
INSERT INTO topic_detail (topic_Id, description) VALUES (180,'OAuth 2.0 - Client Credentials');

/*Tools & Operations*/

/*TSLint Usage*/


INSERT INTO topic_detail (topic_Id, url, description) VALUES (181,'https://palantir.github.io/tslint/usage/cli/','CLI');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (181,'https://palantir.github.io/tslint/usage/library/','Library');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (181,'https://palantir.github.io/tslint/usage/configuration/','Configuration');

/*TSLint Rules*/
INSERT INTO topic_detail (topic_Id, url, description) VALUES (182,'https://palantir.github.io/tslint/rules#typescript-specific','TypeScript-specific');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (182,'https://palantir.github.io/tslint/rules#functionality','Functionality');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (182,'https://palantir.github.io/tslint/rules#maintainability','Maintainability');

/*Gulp Basics*/

INSERT INTO topic_detail (topic_Id, description) VALUES (183,'Concepts');
INSERT INTO topic_detail (topic_Id, description) VALUES (183,'JavaScript and Gulpfiles');

/*Docker*/

INSERT INTO topic_detail (topic_Id, url, description) VALUES (186,'https://docs.docker.com/get-started/part1','Orientation');
INSERT INTO topic_detail (topic_Id, url, description) VALUES (186,'https://docs.docker.com/get-started/part2','Containers');
