/*Java*/

INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (1,1,1,'First');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (1,1,2,'Third');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (1,1,3,'Forth');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (1,1,4,'Forth');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (1,1,5,'Fifth');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (1,1,6,'Sixth');

/*JVM*/
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (1,2,7,'Forth');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (1,2,8,'Fifth');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (1,2,9,'Fifth');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (1,2,10,'Sixth');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (1,2,11,'Seventh');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (1,2,12,'Eight');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (1,2,13,'Ninth');

/*Library*/
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (1,3,14,'Second');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (1,3,15,'Third');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (1,3,16,'Third');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (1,3,17,'Forth');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (1,3,18,'Fifth');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (1,3,19,'Fifth');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (1,3,20,'Fifth');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (1,3,21,'Sixth');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (1,3,22,'Sixth');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (1,3,23,'Sixth');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (1,3,24,'Seventh');

/*Web*/
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (1,4,25,'Second');

/*Markup Languages*/
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (1,5,26,'Third');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (1,5,27,'Third');

/*Spring Framework*/

INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (1,6,28,'Third');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (1,6,29,'Forth');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (1,6,30,'Forth');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (1,6,31,'Forth');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (1,6,32,'Fifth');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (1,6,33,'Sixth');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (1,6,34,'Eight');


/*Spring Security*/

INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (1,7,35,'Fifth');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (1,7,36,'Fifth');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (1,7,37,'Sixth');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (1,7,38,'Sixth');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (1,7,39,'Sixth');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (1,7,40,'Seventh');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (1,7,41,'Eight');

/*Testing*/

INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (1,23,42,'Third');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (1,23,43,'Forth');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (1,23,44,'Forth');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (1,23,45,'Fifth');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (1,23,46,'Fifth');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (1,23,47,'Sixth');


/*Caching*/

INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (1,8,48,'Forth');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (1,8,49,'Forth');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (1,8,50,'Fifth');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (1,8,51,'Fifth');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (1,8,52,'Sixth');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (1,8,53,'Sixth');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (1,8,54,'Seventh');

/*MEssaging*/

INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (1,9,55,'Fifth');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (1,9,56,'Fifth');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (1,9,57,'Sixth');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (1,9,58,'Sixth');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (1,9,59,'Eight');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (1,9,60,'Eight');

/*Persistence*/
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (1,10,61,'Second');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (1,10,62,'Third');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (1,10,63,'Forth');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (1,10,64,'Sixth');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (1,10,65,'Sixth');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (1,10,66,'Seventh');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (1,10,67,'Eight');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (1,10,68,'Ninth');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (1,10,69,'Ninth');

/*Version Control*/
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (1,24,198,'Second');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (1,24,199,'Forth');

/*Operation*/

INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (1,11,72,'Third');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (1,11,73,'Third');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (1,11,74,'Forth');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (1,11,75,'Forth');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (1,11,76,'Fifth');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (1,11,77,'Fifth');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (1,11,78,'Sixth');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (1,11,79,'Eight');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (1,11,80,'Ninth');


/*Cloud*/

INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (1,12,82,'Ninth');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (1,12,83,'Ninth');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (1,12,84,'Ninth');

/*Scriptig*/


INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (1,13,85,'Third');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (1,13,86,'Fifth');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (1,13,87,'Seventh');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (1,13,88,'Eight');


/*HTTP & REST API*/

INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (1,25,89,'Second');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (1,25,90,'Third');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (1,25,91,'Forth');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (1,25,92,'Fifth');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (1,25,93,'Sixth');


/*Network*/

INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (1,26,94,'Forth');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (1,26,95,'Fifth');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (1,26,96,'Sixth');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (1,26,97,'Seventh');

/*DBMS*/

INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (1,27,98,'Fifth');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (1,27,99,'Fifth');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (1,27,100,'Sixth');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (1,27,101,'Sixth');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (1,27,102,'Seventh');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (1,27,103,'Seventh');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (1,27,104,'Eight');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (1,27,105,'Ninth');



/*Data Structure & Algorithms*/

INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (1,28,106,'Forth');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (1,28,107,'Forth');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (1,28,108,'Fifth');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (1,28,109,'Fifth');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (1,28,110,'Fifth');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (1,28,111,'Sixth');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (1,28,112,'Sixth');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (1,28,113,'Sixth');

/*Software Architecture & Design*/

INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (1,29,114,'Forth');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (1,29,115,'Fifth');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (1,29,116,'Fifth');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (1,29,117,'Sixth');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (1,29,118,'Sixth');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (1,29,119,'Sixth');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (1,29,120,'Seventh');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (1,29,121,'Seventh');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (1,29,122,'Eight');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (1,29,123,'Ninth');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (1,29,124,'Ninth');


/*Design Patterns*/

INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (1,30,125,'Third');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (1,30,126,'Third');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (1,30,127,'Third');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (1,30,128,'Forth');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (1,30,129,'Forth');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (1,30,130,'Forth');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (1,30,131,'Fifth');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (1,30,132,'Fifth');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (1,30,133,'Fifth');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (1,30,134,'Sixth');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (1,30,135,'Sixth');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (1,30,136,'Sixth');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (1,30,137,'Seventh');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (1,30,138,'Seventh');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (1,30,139,'Seventh');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (1,30,140,'Eight');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (1,30,141,'Eight');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (1,30,142,'Eight');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (1,30,143,'Ninth');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (1,30,144,'Ninth');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (1,30,145,'Ninth');

/*JavaScript*/

INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (2,14,146,'First');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (2,14,147,'First');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (2,14,148,'Second');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (2,14,149,'Second');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (2,14,150,'Third');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (2,14,151,'Third');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (2,14,152,'Forth');

/*TypeScript*/

INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (2,15,153,'Third');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (2,15,154,'Forth');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (2,15,155,'Fifth');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (2,15,156,'Fifth');

/*Html & Css*/

INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (2,16,157,'First');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (2,16,158,'Second');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (2,16,159,'Third');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (2,16,160,'Forth');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (2,16,161,'Seventh');

/*Frameworkds & LIbraries*/

INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (2,17,162,'Second');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (2,17,163,'Third');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (2,17,164,'Forth');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (2,17,165,'Fifth');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (2,17,166,'Fifth');



/*HTTP & REST API*/

INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (2,25,89,'Second');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (2,25,90,'Third');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (2,25,91,'Forth');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (2,25,92,'Fifth');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (2,25,93,'Sixth');

/*WebSockets*/

INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (2,18,172,'Fifth');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (2,18,173,'Seventh');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (2,18,174,'Seventh');


/*NPM & NodeJs*/

INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (2,19,175,'Third');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (2,19,176,'Forth');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (2,19,177,'Fifth');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (2,19,178,'Sixth');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (2,19,179,'Seventh');

/*Auyhentication*/

INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (2,20,180,'Fifth');

/*Tools & Operations*/
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (2,21,181,'Third');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (2,21,182,'Forth');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (2,21,183,'Forth');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (2,21,184,'Fifth');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (2,21,185,'Fifth');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (2,21,186,'Sixth');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (2,21,187,'Seventh');

/*Webpack*/

INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (2,22,188,'Forth');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (2,22,189,'Forth');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (2,22,190,'Forth');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (2,22,191,'Fifth');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (2,22,192,'Fifth');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (2,22,193,'Fifth');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (2,22,194,'Sixth');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (2,22,195,'Sixth');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (2,22,196,'Sixth');

/*Version Control*/
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (2,24,198,'Second');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (2,24,199,'Forth');

/*Network*/

INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (2,26,94,'Forth');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (2,26,95,'Fifth');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (2,26,96,'Sixth');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (2,26,97,'Seventh');

/*DBMS*/

INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (2,27,98,'Fifth');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (2,27,99,'Fifth');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (2,27,100,'Sixth');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (2,27,101,'Sixth');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (2,27,102,'Seventh');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (2,27,103,'Seventh');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (2,27,104,'Eight');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (2,27,105,'Ninth');


/*Data Structure & Algorithms*/

INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (2,28,106,'Forth');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (2,28,107,'Forth');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (2,28,108,'Fifth');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (2,28,109,'Fifth');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (2,28,110,'Fifth');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (2,28,111,'Sixth');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (2,28,112,'Sixth');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (2,28,113,'Sixth');



/*Software Architecture & Design*/

INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (2,29,114,'Forth');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (2,29,115,'Fifth');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (2,29,116,'Fifth');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (2,29,117,'Sixth');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (2,29,118,'Sixth');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (2,29,119,'Sixth');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (2,29,120,'Seventh');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (2,29,121,'Seventh');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (2,29,122,'Eight');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (2,29,123,'Ninth');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (2,29,124,'Ninth');

/*DEsingn Patterns*/
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (2,30,234,'Fifth');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (2,30,235,'Sixth');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (2,30,236,'Seventh');
INSERT INTO topic_framework (framework_Id, category_Id, topic_Id, level) VALUES (2,30,237,'Eight');




/*
First', 'Second', 'Third', 'Forth', 'Fifth', 'Sixth', 'Seventh', 'Eight', 'Ninth'

*/