package com.synergy.competencyframework.repository;

import com.synergy.competencyframework.domain.Framework;
import com.synergy.competencyframework.domain.TopicFramework;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TopicFrameworkRepository extends JpaRepository<TopicFramework, Long> {
    List<TopicFramework> findByFramework(Framework framework);
}
