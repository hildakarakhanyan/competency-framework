package com.synergy.competencyframework.repository;

import com.synergy.competencyframework.domain.Framework;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FrameworkRepository extends JpaRepository<Framework, Long> {
}
