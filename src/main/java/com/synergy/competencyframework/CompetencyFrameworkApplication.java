package com.synergy.competencyframework;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CompetencyFrameworkApplication {

	public static void main(String[] args) {
		SpringApplication.run(CompetencyFrameworkApplication.class, args);
	}

}
