package com.synergy.competencyframework.controller;

import com.synergy.competencyframework.domain.Category;
import com.synergy.competencyframework.domain.TopicFramework;
import com.synergy.competencyframework.service.FrameworkService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@RestController
@CrossOrigin
@RequestMapping(value = "/framework")
public class FrameworkController {

    private FrameworkService frameworkService;

    @Autowired
    public void setFrameworkService(FrameworkService frameworkService) {
        this.frameworkService = frameworkService;
    }

    @GetMapping(value = "/frameworks")
    public ResponseEntity<?> findAllFrameworks() {
        return ResponseEntity.ok(frameworkService.findAll());
    }

    @GetMapping(value = "/{frameworkId}/topics")
    public ResponseEntity<Map<String, Object>> getAllFrameworkTopics(@PathVariable Long frameworkId) {
        List<TopicFramework> topics = frameworkService.findTopicsByFrameworkId(frameworkId);
        Set<Category> categories = topics.stream().map(TopicFramework::getCategory).collect(Collectors.toSet());
        Map<Long, List<TopicFramework>> topicsByCategory = topics.stream()
                .collect(Collectors.groupingBy(t -> t.getCategory().getId()));
        Map<String, Object> result = new HashMap<>();
        result.put("categories", categories);
        result.put("topicsByCategory", topicsByCategory);
        return ResponseEntity.ok(result);
    }
}
