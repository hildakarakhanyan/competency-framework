package com.synergy.competencyframework.domain;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Entity
@Data
@Table(name = "assessment")
public class Assessment {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name="assessment_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date assessmentDate;

    @Column
    private String description;

    @ManyToOne
    @JoinColumn(name = "user_Id")
    private User user;

    @ManyToOne
    @JoinColumn(name = "state_Id")
    private State state;

    @ManyToOne
    @JoinColumn(name = "topic_framework_Id")
    private TopicFramework topicFramework;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getAssessmentDate() {
        return assessmentDate;
    }

    public void setAssessmentDate(Date assessmentDate) {
        this.assessmentDate = assessmentDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public TopicFramework getTopicFramework() {
        return topicFramework;
    }

    public void setTopicFramework(TopicFramework topicFramework) {
        this.topicFramework = topicFramework;
    }
}
