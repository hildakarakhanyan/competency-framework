package com.synergy.competencyframework.domain;

public enum Level {
    First, Second, Third, Forth, Fifth, Sixth, Seventh, Eight, Ninth
}
