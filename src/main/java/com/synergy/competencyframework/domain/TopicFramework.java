package com.synergy.competencyframework.domain;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "topic_framework")
public class TopicFramework {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "topic_Id")
    private Topic topic;

    @ManyToOne
    @JoinColumn(name = "framework_Id")
    private Framework framework;

    @ManyToOne
    @JoinColumn(name = "category_Id")
    private Category category;

    @Enumerated(EnumType.STRING)
    private Level level;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Topic getTopic() {
        return topic;
    }

    public void setTopic(Topic topic) {
        this.topic = topic;
    }

    public Framework getFramework() {
        return framework;
    }

    public void setFramework(Framework framework) {
        this.framework = framework;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Level getLevel() {
        return level;
    }

    public void setLevel(Level level) {
        this.level = level;
    }
}
