package com.synergy.competencyframework.service;

import com.synergy.competencyframework.domain.Framework;
import com.synergy.competencyframework.domain.TopicFramework;
import com.synergy.competencyframework.repository.FrameworkRepository;
import com.synergy.competencyframework.repository.TopicFrameworkRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class FrameworkService {

    private FrameworkRepository frameworkRepository;

    @Autowired
    public void setTopicFrameworkRepository(TopicFrameworkRepository topicFrameworkRepository) {
        this.topicFrameworkRepository = topicFrameworkRepository;
    }

    private TopicFrameworkRepository topicFrameworkRepository;

    @Autowired
    public void setFrameworkTypeRepository(FrameworkRepository frameworkRepository) {
        this.frameworkRepository = frameworkRepository;
    }

    public List<Framework> findAll() {
        return frameworkRepository.findAll();
    }

    public List<TopicFramework> findTopicsByFrameworkId(Long id) {
        Optional<Framework> framework = frameworkRepository.findById(id);
        if(framework.isPresent()) {
            return topicFrameworkRepository
                    .findByFramework(framework.get());
        }
        throw new IllegalStateException();
    }
}